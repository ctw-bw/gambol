#!/bin/bash

cd build

g++ -std=c++14 -I/usr/include/eigen3 -Wall -fmessage-length=0 -pthread -L/usr/local/lib/mujoco ../src/*.cpp ../src/*/*.cpp -lmujoco200 -lGL -lglew -lglfw -lifopt_core -lifopt_ipopt -pg -o Gambol.a
