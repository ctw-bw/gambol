#ifndef GAMBOL_MODELS_BIPED_GAIT_GENERATOR_H_
#define GAMBOL_MODELS_BIPED_GAIT_GENERATOR_H_

#include "GaitGenerator.h"

namespace gambol {

/**
 * @brief Produces the contact sequence for a variety of two-legged gaits.
 *
 * @sa GaitGenerator for more documentation
 */
    class GaitGeneratorBiped : public GaitGenerator {
    public:
        GaitGeneratorBiped();

        virtual ~GaitGeneratorBiped() = default;

        enum BipedIDs {
            L, R
        };

    protected:
        virtual GaitInfo GetGait(Gaits gait) const override;

        GaitInfo GetStrideStand() const;

        GaitInfo GetStrideFlight() const;

        GaitInfo GetStrideWalk() const;

        GaitInfo GetStrideWalkBegin() const;

        GaitInfo GetStrideWalkEnd() const;

        GaitInfo GetStrideRun() const;

        GaitInfo GetStrideHop() const;

        GaitInfo GetStrideLeftHop() const;

        GaitInfo GetStrideRightHop() const;

        GaitInfo GetStrideGallopHop() const;

        void SetCombo(Combos combo) override;

        // naming convention:, where the circle is is contact, front is right ->.
        ContactState I_; // flight
        ContactState b_; // right-leg in contact
        ContactState P_; // left leg in contact
        ContactState B_; // stance (both legs in contact)
    };

} /* namespace gambol */

#endif /* GAMBOL_MODELS_BIPED_GAIT_GENERATOR_H_ */
