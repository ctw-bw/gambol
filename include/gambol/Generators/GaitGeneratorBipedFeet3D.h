#ifndef GENERATORS_GAITGENERATORBIPEDFEET3D_H_
#define GENERATORS_GAITGENERATORBIPEDFEET3D_H_

#include "GaitGeneratorBipedFeet.h"

namespace gambol {

/**
 * @brief Produces the contact sequence for a variety of two-legged gaits including heel and toe in 3D
 *
 * Each foot has three contact points, but the two in the two are considered congruent.
 *
 * Modes and strides are inherited from GaitGeneratorBipedFeet.
 *
 * @sa GaitGenerator for more documentation
 */
    class GaitGeneratorBipedFeet3D : public GaitGeneratorBipedFeet {
    public:
        GaitGeneratorBipedFeet3D();

        virtual ~GaitGeneratorBipedFeet3D() = default;

        enum BipedFeetIDs {
            // Left and right, heel and toe, toe inside and toe outside
            LH,
            LTO,
            LTI,
            RH,
            RTO,
            RTI
        };
    };

} /* namespace gambol */

#endif /* GENERATORS_GAITGENERATORBIPEDFEET3D_H_ */
