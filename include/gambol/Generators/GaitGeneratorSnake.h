#ifndef GAMBOL_MODELS_SNAKE_GAIT_GENERATOR_H_
#define GAMBOL_MODELS_SNAKE_GAIT_GENERATOR_H_

#include "GaitGenerator.h"

namespace gambol {

/**
 * Produces the contact sequence for a 4-link snake
 *
 * Snake will have three contact points
 *
 * @sa GaitGenerator for more documentation
 */
    class GaitGeneratorSnake : public GaitGenerator {
    public:
        GaitGeneratorSnake();

        virtual ~GaitGeneratorSnake() = default;

    private:
        GaitInfo GetGait(Gaits gait) const override;

        GaitInfo GetStrideStand() const;

        GaitInfo GetStrideFlight() const;

        GaitInfo GetStrideCrawl() const;

        GaitInfo GetStrideCrawlMiddle() const;

        GaitInfo GetStrideHop() const;

        void SetCombo(Combos combo) override;

        // naming convention: where the circle is, is contact, front is right ->.
        ContactState xxx_; // Flight
        ContactState xxo_; // Tip is in contact
        ContactState xox_; // Middle is in contact
        ContactState oxx_; // Tail is in contact
        ContactState xoo_; // Tail is in the air
        ContactState oxo_; // Middle is in the air
        ContactState oox_; // Tip is in the air
        ContactState ooo_; // Stance
    };

} /* namespace gambol */

#endif /* GAMBOL_MODELS_SNAKE_GAIT_GENERATOR_H_ */
