#ifndef PROBLEM_NLPFORMULATION_H_
#define PROBLEM_NLPFORMULATION_H_

#include <ifopt/constraint_set.h>
#include <ifopt/cost_term.h>
#include <ifopt/ipopt_solver.h>

#include "Parameters.h"
#include <gambol/Variables/NodesHolder.h>
#include <gambol/Variables/NodesVariables.h>
#include <gambol/Variables/NodeTimes.h>
#include <gambol/Variables/PhaseDurations.h>
#include <gambol/Robots/RobotModel.h>
#include <gambol/Terrain/HeightMap.h>

namespace gambol {

    /**
     * A combination of variablesets and constraints
     *
     * This class is basically a factory for NLP, to be solved by IFOPT.
     */
    class NlpFormulation {
    public:
        using VariablePtrVec = std::vector<ifopt::VariableSet::Ptr>;
        using ContraintPtrVec = std::vector<ifopt::ConstraintSet::Ptr>;
        using CostPtrVec = std::vector<ifopt::CostTerm::Ptr>;
        using VectorXd = Eigen::VectorXd;
        using Vector3d = Eigen::Vector3d;
        using Vector4d = Eigen::Vector4d;

        /**
         * Constructor
         */
        NlpFormulation();

        /**
         * Destructor
         */
        virtual ~NlpFormulation() = default;

        /**
         * Get variables to be optimised over
         */
        VariablePtrVec GetVariableSets(NodesHolder& nodes_holder);

        /**
         * Return set of all constraints
         */
        ContraintPtrVec GetConstraints(const NodesHolder& nodes_holder) const;

        /**
         * Return set of all costs
         */
        ContraintPtrVec GetCosts() const;

        /**
         * Set solver options
         *
         * `max_iter` and `max_cpu_time` still need to be set outside this function.
         *
         * `tol`				-	Desired convergence tolerance, scaled NLP error must
         * 							be less than this
         * `constr_viol_tol`	-	Max. unscaled constr. violation must be less than this
         * `dual_inf_tol`		-	Max. unscaled dual infeasibility must be less than this
         * `compl_inf_tol`		-	Max. unscaled complementarity must be less than this
         *
         * `acceptable_*`		-	Same as above but for acceptable termination
         */
        static void setSolverOptions(const ifopt::IpoptSolver::Ptr& solver);

        /** Parameters of optimisation */
        Parameters params_;

        /** Underlying robot model */
        RobotModel::Ptr model_;

        /** Terrain object */
        HeightMap::Ptr terrain_;

        int size_q_;    ///< Number of states
        int size_dq_;    ///< Number of velocity/acceleration states
        int size_u_;    ///< Number of torque inputs

        VectorXd initial_joint_pos_, initial_joint_vel_;    ///< Initial joint position
        VectorXd final_joint_pos_, final_joint_vel_;        ///< Final joint position
        std::vector<int> bound_initial_joint_pos_, bound_initial_joint_vel_;///< Indices of joints to bound
        std::vector<int> bound_final_joint_pos_, bound_final_joint_vel_; ///< Indices of joints to bound

        std::vector<std::pair<double, double>> joint_pos_limits_; ///< Joint limits
        std::vector<int> bound_joint_pos_limits_; ///< Indices of joints which to limit
        std::vector<std::pair<double, double>> torque_limits_; ///< Torque limits
        std::vector<int> bound_torque_limits_; ///< Indices of torques to limit
        std::vector<int> bound_symmetry_joint_pos_; ///< Indices of joint to fix for symmetry

        bool initial_zpos_; ///< True of initial z-pos is bound

        NodeTimes::Ptr node_times_; ///< Original node_times object
        std::vector<PhaseDurations::Ptr> phase_durations_; ///< Original phases object

    private:

        /**
         * Return joint variables for position and velocity
         */
        std::vector<NodesVariables::Ptr> MakeVariablesJoints() const;

        /**
         * Return joint variables for joint torques
         */
        NodesVariables::Ptr MakeVariablesTorques() const;

        /**
         * Return joint variables for end-effector forces
         *
         * Return vector with variable for each end-effector.
         */
        std::vector<NodesVariables::Ptr> MakeVariablesForces() const;

        /**
         * Return shared NodeTimes object
         */
        NodeTimes::Ptr MakeNodeTimes() const;

        /**
         * Return shared PhaseDurations object
         */
        std::vector<PhaseDurations::Ptr> MakePhaseDurations() const;

        /**
         * Get specific constraint through name
         */
        ContraintPtrVec GetConstraint(Parameters::ConstraintName name,
                                      const NodesHolder& splines) const;

        /**
         * Get velocity integration constraint
         */
        ContraintPtrVec MakeConstraintIntegration(const NodesHolder& s) const;

        /**
         * Get quaternion constraint
         */
        ContraintPtrVec MakeConstraintQuaternions(const NodesHolder& s) const;

        /**
         * Get velocity integration constraint
         */
        ContraintPtrVec MakeConstraintDynamics(const NodesHolder& s) const;

        /**
         * Get terrain constraint
         */
        ContraintPtrVec MakeConstraintTerrain(const NodesHolder& s) const;

        /**
         * Get forces constraint
         */
        ContraintPtrVec MakeConstraintForces(const NodesHolder& s) const;

        /**
         * Get symmetry constraint
         *
         * Use `bound_symmetry_joint_pos_` to determine which positions should be
         * constraint.
         * All velocities and torques are constraint by default.
         */
        ContraintPtrVec MakeConstraintSymmetry(const NodesHolder& s) const;

        /**
         * Get a single cost based on name and weight
         */
        CostPtrVec GetCost(const Parameters::CostName& id, double weight) const;

        /**
         * Make torque cost
         */
        CostPtrVec MakeCostTorque(double weight) const;

        /**
         * Make joint acceleration cost
         */
        CostPtrVec MakeCostJointAcceleration(double weight) const;

        /**
         * Make cost for angular velocity of main body
         */
        CostPtrVec MakeCostAngularVelocity(double weight) const;

        /**
         * Make foot-lift cost
         */
        CostPtrVec MakeCostFootLift(double weight) const;

        /**
         * Set joint initial guess
         *
         * Based on initial and final positions and robot model.
         */
        void SetJointsInitialGuess(NodesHolder& nodes_holder) const;
    };

} // namespace gambol

#endif /* PROBLEM_NLPFORMULATION_H_ */
