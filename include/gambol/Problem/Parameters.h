#ifndef PROBLEM_PARAMETERS_H_
#define PROBLEM_PARAMETERS_H_

#include <vector>

namespace gambol {

    /**
     * Parameters to control the optimisation
     */
    class Parameters {
    public:
        enum ConstraintName {
            Integration,            ///< Velocity to position integration
            Quaternions,            ///< Unity quaternion constraint
            Dynamics,                ///< Acceleration integration
            Terrain,                ///< Terrain constraint
            Forces,                    ///< Forces constraint
            Symmetry                ///< Make last pose equal to initial pose
        };

        enum CostName {
            Torque,                ///< Cost on torque nodes
            JointAcceleration,    ///< Cost on joint accelerations
            AngularVelocity,    ///< Angular velocity of base
            FootLiftReward        ///< Award lifting the foot above the ground
        };

        using VecTimes = std::vector<double>;
        using UsedConstraints = std::vector<ConstraintName>;
        using UsedCosts = std::vector<std::pair<CostName, double>>;

        /**
         * Constructor
         */
        Parameters();

        /**
         * Destructor
         */
        virtual ~Parameters() = default;

        /**
         * Return sum of phase durations
         *
         * Also assert the sum for each end-effector is equal
         */
        double GetTotalTime() const;

        /**
         * Return number of end-effectors
         */
        uint GetEECount() const;

        /**
         * Return linspaced integer vector
         */
        static std::vector<int> GetLinspaceVector(int n, int start = 0,
                                                  int step = 1);

        UsedConstraints constraints_; ///< Names of constraints to be used
        UsedCosts costs_; ///< Names of costs to be used

        int N_;            ///< Number of collocation points
        double t_total;    ///< Total duration of optimisation

        std::vector<bool> initial_contact_;
        std::vector<VecTimes> ee_phase_durations_; ///< Phase duration of each end-effector

        double max_normal_force_;

        bool symmetry_; // Set to `true` to make last pose identical to first pose
    };

} /* namespace gambol */

#endif /* PROBLEM_PARAMETERS_H_ */
