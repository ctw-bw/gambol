#ifndef PROBLEM_NLPTOFILE_H_
#define PROBLEM_NLPTOFILE_H_

#include <string>
#include <ifopt/problem.h>

namespace gambol {

    /**
     * Little helper class to for saving a NLP to file
     *
     * The class can save the optimization outcome to a file, and
     * use such a previous outcome for a next optimization (iff the
     * NLP is the same).
     */
    class NLPToFile {
    public:

        /**
         * Constructor
         */
        NLPToFile(ifopt::Problem* nlp, const std::string& filename);

        virtual ~NLPToFile() = default;

        /**
         * Set guess form file
         */
        bool SetGuessFromFile();

        /**
         * Write current variable values of NLP to file
         */
        bool WriteFileFromResult() const;

    private:
        std::string filename_;
        ifopt::Problem* nlp_;
    };

} /* namespace gambol */

#endif /* PROBLEM_NLPTOFILE_H_ */
