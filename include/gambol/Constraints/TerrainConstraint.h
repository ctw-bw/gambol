#ifndef CONSTRAINTS_TERRAINCONSTRAINT_H_
#define CONSTRAINTS_TERRAINCONSTRAINT_H_

#include "NodesConstraint.h"
#include <gambol/Robots/RobotModel.h>
#include <gambol/Terrain/HeightMap.h>

namespace gambol {

    /**
     * Terrain constraint
     *
     * Keeps end-effector on the ground with velocity zero during stance
     * and keeps it above ground during swing.
     *
     * 		| '	|	|	| '	|
     * 		| '	|	|	| '	x
     * 		x '	|	|	| '	|
     * 		| '	|	|	| '	|
     * 		| '	x	x	x '	|
     * 		| '	|	|	| '	|
     * 	(sw)	   (st)	     (sw)
     *
     * The constraints for each node are:
     * 			z_ee_k
     * 		x_ee_k - x_ee_k-1
     * 		y_ee_k - y_ee_k-1
     *
     * During swing the bounds are (0,inf) and (-inf,inf) respectively.
     * During stance the bounds are (0,0) for both.
     * _Except_ for the first node of a stance phase! For this node the
     * x-position constraint is also (-inf,inf).
     */
    class TerrainConstraint : public NodesConstraint {
    public:
        using Vector3d = Eigen::Vector3d;

        /**
         * Constructor
         *
         * @param terrain			Pointer to heightmap
         * @param model				Pointer to robot model
         * @param nodes_holder		Collection of variables
         * @param ee_id				End-effector this constraint applies to
         * @param skip_initial		Set to true to not constraint the first frame
         */
        TerrainConstraint(const HeightMap::Ptr& terrain, const RobotModel::Ptr& model,
                          const NodesHolder& nodes_holder, uint ee_id, bool skip_initial = false);

        ~TerrainConstraint() override = default;

        /**
         * Update state properties for current node
         *
         * @return bool		False if this k should be skipped
         */
        bool Update(int k) const;

        /**
         * Get constraint values
         */
        void UpdateConstraintAtNode(int k, VectorXd& g) const override;

        /**
         * Get constraint bounds
         */
        void UpdateBoundsAtNode(int k, VecBound& b) const override;

        /**
         * Fill jacobian for constraint
         */
        void UpdateJacobianAtNode(int k, std::string var_set,
                                          Jacobian& jac) const override;

    private:


        /**
         * Get row of constraint vector based on node info
         */
        int GetRow(int k, int type) const;

        uint ee_id_;
        bool skip_initial_;
        HeightMap::Ptr terrain_; ///< Terrain used as height map
        mutable RobotModel::Ptr model_; ///< Robot model used for kinematics
        mutable RobotModel::Ptr model_km1_;
    };

} /* namespace gambol */

#endif /* CONSTRAINTS_TERRAINCONSTRAINT_H_ */
