#ifndef CONSTRAINTS_NODESCONSTRAINT_H_
#define CONSTRAINTS_NODESCONSTRAINT_H_

#include <ifopt/constraint_set.h>

#include <gambol/Variables/NodesHolder.h>

namespace gambol {

    /**
     * Constraints evaluated at each node in a trajectory
     */
    class NodesConstraint : public ifopt::ConstraintSet {
    public:
        using VectorXd = Eigen::VectorXd;
        using Vector3d = Eigen::Vector3d;
        using MatrixXd = Eigen::MatrixXd;
        using VecTimes = std::vector<double>;
        using Bounds = ifopt::Bounds;

        /**
         * Return vector of constraint values
         */
        VectorXd GetValues() const override;

        /**
         * Return vector of constraint bounds
         */
        VecBound GetBounds() const override;

        /**
         * Return complete constraint jacobian
         */
        void FillJacobianBlock(std::string var_set, Jacobian&) const override;

    protected:

        /**
         * Constructor.
         *
         * Constructor is protected to make the class abstract.
         */
        NodesConstraint(const NodesHolder& s, const std::string& name);

        ~NodesConstraint() override = default;

        /**
         * Return number of nodes
         */
        int GetNumberOfNodes() const;

        NodesHolder nodes_holder_;

    private:

        /**
         * @brief Sets the constraint value a node k
         * @param k  The index of the time t, so t=k*dt
         * @param[in,out] g  The complete vector of constraint values, for which the
         *                   corresponding row must be filled.
         */
        virtual void UpdateConstraintAtNode(int k, VectorXd& g) const = 0;

        /**
         * @brief Sets upper/lower bound to node k
         * @param k  The index of the time t, so t=k*dt
         * @param[in,out] b The complete vector of bounds, for which the corresponding
         *                  row must be set.
         */
        virtual void UpdateBoundsAtNode(int k, VecBound& b) const = 0;

        /**
         * @brief Sets Jacobian rows at a specific node k
         * @param k  The index of the time t, so t=k*dt
         * @param var_set The name of the ifopt variables currently being queried for.
         * @param[in,out] jac  The complete Jacobian, for which the corresponding
         *                     row and columns must be set.
         */
        virtual void UpdateJacobianAtNode(int k, std::string var_set,
                                          Jacobian& jac) const = 0;
    };

} /* namespace gambol */

#endif /* CONSTRAINTS_NODESCONSTRAINT_H_ */
