#ifndef CONSTRAINTS_SYMMETRYCONSTRAINT_H_
#define CONSTRAINTS_SYMMETRYCONSTRAINT_H_

#include <ifopt/constraint_set.h>

#include <gambol/Variables/NodesVariables.h>

namespace gambol {

    /**
     * Constraint to keep the initial pose identical the last pose.
     *
     * This constraint is relevant for the first and last node. So extending `NodesConstraint` is not
     * convenient.
     */
    class SymmetryConstraint : public ifopt::ConstraintSet {
    public:
        using VectorXd = Eigen::VectorXd;
        using VecTimes = std::vector<double>;
        using Bounds = ifopt::Bounds;

        /**
         * Constructor
         *
         * @param nodes		Pointer to NodesVariables to constraint
         * @param dims		List of dimensions to constrain (constrain all
         * 					when left empty)
         */
        explicit SymmetryConstraint(const NodesVariables::Ptr& nodes, const std::vector<int>& dims = {});

        ~SymmetryConstraint() override = default;

        /**
         * Get constraint values
         */
        VectorXd GetValues() const override;

        /**
         * Get bounds of the constraint
         */
        VecBound GetBounds() const override;

        /**
         * Fill in jacobian for the constraint
         */
        void FillJacobianBlock(std::string var_set, Jacobian& jac) const override;

    private:

        NodesVariables::Ptr nodes_;
        std::vector<int> dims_;
    };

} /* namespace gambol */

#endif /* CONSTRAINTS_SYMMETRYCONSTRAINT_H_ */
