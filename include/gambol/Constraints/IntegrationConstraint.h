#ifndef CONSTRAINTS_INTEGRATIONCONSTRAINT_H_
#define CONSTRAINTS_INTEGRATIONCONSTRAINT_H_

#include "NodesConstraint.h"
#include <gambol/Robots/RobotModel.h>

namespace gambol {

    /**
     * Constraint to ensure velocity between steps integrates to position
     *
     * Constraint value is integration error:
     *
     * 		q[k] - q[k+1] + dt/2 * (dq[k] + dq[k+1])
     *
     * Note that for some models v != dq/dt. Hence a jacobian is used from
     * the robot model:
     *
     *		q[k] - q[k+1] + dt/2 * (B[k] * v[k] + B[k+1] * v[k+1])
     *
     * Where
     *
     * 		dq(t)/dt = B(t) * v(t)
     */
    class IntegrationConstraint : public NodesConstraint {
    public:
        using MatrixXd = Eigen::MatrixXd;

        /**
         * Constructor
         */
        IntegrationConstraint(const RobotModel::Ptr& model,
                              const NodesHolder& nodes_holder);

        ~IntegrationConstraint() override = default;

    private:

        /**
         * Get row of constraint vector based on node info
         */
        int GetRow(int k, int dim = 0) const;

        /**
         * Update state properties for current node
         *
         * @return bool		False if k is the last node
         */
        bool Update(int k) const;

        /**
         * Update constraint value at node
         */
        void UpdateConstraintAtNode(int k, VectorXd& g) const override;

        /**
         * Set bound for specific node
         */
        void UpdateBoundsAtNode(int k, VecBound& b) const override;

        /**
         * Set jacobian for this node
         *
         * `jac` is the jacobian of the entire constraint to the current variable
         * set.
         */
        void UpdateJacobianAtNode(int k, std::string var_set,
                                  Jacobian& jac) const override;

        int size_q_, size_dq_;

        // `mutable` to ignore const flag
        mutable double dt_k_;
        mutable RobotModel::Ptr model_k_; ///< Robot model on which dynamics is based
        mutable RobotModel::Ptr model_kp1_; ///< Robot model on which dynamics is based
    };

} /* namespace gambol */

#endif /* CONSTRAINTS_INTEGRATIONCONSTRAINT_H_ */
