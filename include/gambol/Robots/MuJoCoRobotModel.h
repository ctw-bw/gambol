#ifndef ROBOTS_MUJOCOROBOTMODEL_H_
#define ROBOTS_MUJOCOROBOTMODEL_H_

#include <Eigen/Dense>
#include <vector>
#include <string>
#include "RobotModel.h"
#include <gambol/Variables/NodesHolder.h>
#include <MuJoCoTools/MuJoCoModel.h>

namespace gambol {

    /**
     * Robot model defined through MuJoCo
     *
     * Use the ee-names to mark the end-effectors. The names must be body names
     * and the body frame must be at the tip of the end-effector.
     */
    class MuJoCoRobotModel : public RobotModel {
    public:

        /**
         * Constructor
         *
         * size_q and size_u can also be extracted from the model, however,
         * the parent constructor already requires them.
         */
        MuJoCoRobotModel(const std::string& file, int size_q, int size_dq,
                         int size_u, const std::vector<std::string>& ee_names);

        /**
         * Define copy-constructor as default
         */
        MuJoCoRobotModel(const MuJoCoRobotModel&) = default;

        /**
         * Clone object
         */
        MuJoCoRobotModel::Ptr clone() const override;

        /**
         * Destructor
         */
        virtual ~MuJoCoRobotModel() = default;

        /**
         * Check if base was defined
         */
        bool HasIK() const override;

        /**
         * Return total mass of robot
         */
        double GetTotalMass() const override;

        /**
         * Return gravitational constant
         */
        double GetGravity() const override;

        /**
         * Return vector of MuJoCo body ids of the end-effectors
         */
        const std::vector<int>& GetEEBodyIds() const;

        /**
         * Get current dynamics
         */
        VectorXd GetDynamics() const override;

        /**
         * Get dynamics jacobian w.r.t position
         */
        Jacobian GetDynamicsJacobianWrtPos() const override;

        /**
         * Get dynamics jacobian w.r.t velocity
         */
        Jacobian GetDynamicsJacobianWrtVel() const override;

        /**
         * Get dynamics jacobian w.r.t torque
         */
        Jacobian GetDynamicsJacobianWrtTorque() const override;

        /**
         * Get dynamics jacobian w.r.t. forces
         */
        Jacobian GetDynamicsJacobianWrtForces(uint ee_id) const override;

        /**
         * Get position of specific end-effector
         */
        Vector3d GetEEPos(uint ee_id) const override;

        /**
         * Get jacobian of end-effector position to joint positions
         *
         * Jacobian is the position jacobian:
         *
         * \f[
         * 		\dot{p} = J_q * \dot{qpos}
         * \f]
         */
        Jacobian GetEEPosJacobian(uint ee_id) const override;

        /**
         * Get position of base
         */
        Vector3d GetBasePos() const override;

        /**
         * Get jacobian of base position to joint positions
         */
        Jacobian GetBasePosJacobian() const override;

        /**
         * Solve IK problem
         */
        VectorXd GetInverseKinematics(const Vector3d& base_pos,
                                      const Vector4d& base_rot,
                                      const std::vector<Vector3d>& ee_pos) const override;

        /**
         * Get reference to MuJoCo model
         */
        MuJoCoModel& GetMuJoCoModel();

    protected:

        /**
         * Update internal calculations
         */
        void Update() override;

        /**
         * MuJoCo model object
         *
         * `mutable` since it's internal states are to be updated,
         * regardless of const flags.
         */
        mutable MuJoCoModel model_;

        std::vector<int> ee_body_id_; ///< Body ids of end-effectors
        int base_body_id_; ///< Body id of the base
    };

} /* namespace gambol */

#endif /* ROBOTS_MUJOCOROBOTMODEL_H_ */
