#ifndef ROBOTS_ROBOT_H_
#define ROBOTS_ROBOT_H_

#include <MuJoCoTools/MuJoCoModel.h>
#include "RobotModel.h"
#include <gambol/Generators/GaitGenerator.h>

namespace gambol {

/**
 * Wrapper structure for robots
 *
 * A `Robot` instance should make it easy to swap models in the same
 * optimization.
 * However, it should not be essential!
 */
    class Robot {
    public:

        using VectorXd = Eigen::VectorXd;
        using Vector3d = Eigen::Vector3d;
        using Vector4d = Eigen::Vector4d;
        using Limits = std::vector<std::pair<double, double>>;
        using Bounds = std::vector<int>;

        /**
         * Pre-defined robotic systems for optimization.
         */
        enum RobotName {
            Monoped,        ///< Monoped, one-legged hopper (free base joint)
            Monoped3D,      ///< Monoped in 3D (pinfoot)
            Monoped3DEuler, ///< Monoped in 3D (pinfoot, with Euler angles)
            MonopedFoot,    ///< Monoped but with free foot joint instead
            Biped,          ///< Two-legged 5-link biped
            Biped3D,        ///< Two-legged 5-link biped in 3D (with a freejoint)
            BipedFeet,      ///< Biped with toes
            BipedFeet3D,    ///< Biped with toes in 3D
            BipedArms3D,    ///< Biped with arms in 3D
            ExoSkeleton,    ///< A basic model of the BME Exo
            Quadruped,      ///< Quadruped in 2D
            Quadruped3D,    ///< Quadruped in 3D
            Block,          ///< A simple legless block
            CartPole,       ///< Pendulum on a cart
            SnakeBot,       ///< 4-link snake
            RaiSimBlock,    ///< A simple legless block in RaiSimLib instead of MuJoCo
            RaiSimCartPole, ///< Pendulum on a cart, made in RaiSim
            RaiSimMonoped,  ///< Monoped in 2D, made in RaiSim
            RaiSimBiped,    ///< Two-legged 5-link biped in RaiSim
            RaiSimBiped3D,  ///< Two-legged 5-link biped in 3D (with a floating base) in RaiSim
            RaiSimWE2,      ///< WE2 URDF model, in RaiSim
        };

        /**
         * Default constructor
         */
        Robot();

        /**
         * Constructor for a listed Robot
         *
         * @param robot
         */
        Robot(RobotName robot);

        /** Destructor */
        virtual ~Robot() = default;

        /** Read joint limits from xml file */
        static void JointLimitsFromMuJoCo(const MuJoCoModel& model,
                                          Limits& limits, Bounds& bounds);

        /** Read torque limits from xml file */
        static void TorqueLimitsFromMuJoCo(const MuJoCoModel& model,
                                           Limits& limits, Bounds& bounds);

        RobotModel::Ptr robot_model_;
        int size_q_, size_dq_, size_u_, num_ee_;

        Limits joint_pos_limits_;
        Bounds bound_joint_pos_limits_;

        VectorXd basic_joint_pos_;

        Limits torque_limits_;
        Bounds bound_torque_limits_;

        std::vector<std::vector<double>> ee_phase_durations_;
        std::vector<bool> initial_contact_;

        Bounds bound_initial_joint_pos_;
        Bounds bound_initial_joint_vel_;

        Bounds bound_symmetry_joint_pos_;

        bool initial_zpos_;

        GaitGenerator::Ptr generator_;

    private:
        void MakeMonoped();

        void MakeMonoped3D();

        void MakeMonoped3DEuler();

        void MakeMonopedFoot();

        void MakeBiped();

        void MakeBiped3D();

        void MakeBipedFeet();

        void MakeBipedFeet3D();

        void MakeBipedArms3D();

        void MakeExoSkeleton();

        void MakeQuadruped();

        void MakeQuadruped3D();

        void MakeBlock();

        void MakeCartPole();

        void MakeSnakeBot();

        void MakeRaiSimBlock();

        void MakeRaiSimCartPole();

        void MakeRaiSimMonoped();

        void MakeRaiSimBiped();

        void MakeRaiSimBiped3D();

        void MakeRaiSimWE2();
    };

} /* namespace gambol */

#endif /* ROBOTS_ROBOT_H_ */
