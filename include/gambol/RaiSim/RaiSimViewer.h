#ifndef SRC_RAISIM_RAISIMVIEWER_H_
#define SRC_RAISIM_RAISIMVIEWER_H_

#include <iostream>
#include <string>
#include <raisim/OgreVis.hpp>

#include <gambol/Robots/RaiSimRobotModel.h>

namespace gambol {

    /**
     * Class to create a viewer for a RaiSim world
     */
    class RaiSimViewer {

    public:

        /**
         * Default constructor
         */
        RaiSimViewer();

        /**
         * Constructor
         */
        RaiSimViewer(raisim::World& world);

        /**
         * Constructor based on RobotModel
         */
        RaiSimViewer(const RaiSimRobotModel::Ptr& model);

        /**
         * Destructor
         */
        ~RaiSimViewer();

        /**
         * Get pointer to global visual object
         */
        raisim::OgreVis* get_vis();

        /**
         * Run simulation and visualization, until the window is closed
         */
        void run();

        /**
         * Callback when application is ready (internal method)
         */
        static void setup_callback();
    };

}

#endif //SRC_RAISIM_RAISIMVIEWER_H_
