#ifndef CONTROL_CSVLOGGER_H_
#define CONTROL_CSVLOGGER_H_

#include <fstream>
#include <string>
#include <vector>

#include <Eigen/Dense>

namespace gambol {

    /**
     * Class to log simulation data to file.
     *
     * It is made to log during an online MPC scheme, so
     * it logs both optimized trajectories and simulated
     * data when this trajectory is applied.
     */
    class CsvLogger {
    public:
        using VectorXd = Eigen::VectorXd;

        /**
         * Constructor.
         *
         * @param name		Name for part of the file, e.g. model name
         */
        explicit CsvLogger(const std::string& name = "");

        /**
         * Filestream is RAII and closes itself.
         */
        virtual ~CsvLogger() = default;

        /**
         * Create log entry.
         *
         * This corresponds to a single row in the file.
         */
        void log(double t, const VectorXd& qpos, const VectorXd& qvel,
                 const VectorXd& qacc, const VectorXd& qpos_r,
                 const VectorXd& qvel_r, const VectorXd& u_ff, const VectorXd& u_fb,
                 const std::vector<VectorXd>& forces,
                 const std::vector<VectorXd>& forces_r, int predict_count = 0);

        /**
         * Return file name from a path.
         */
        static std::string fileNameFromPath(std::string path);

    private:
        std::ofstream log_file_;
        int rows_;
    };

} /* namespace gambol */

#endif /* CONTROL_CSVLOGGER_H_ */
