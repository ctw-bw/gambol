#ifndef VARIABLES_VARIABLESNAMES_H_
#define VARIABLES_VARIABLESNAMES_H_

#include <string>

namespace gambol {

    /**
     * Provide constants to recognize the variable names
     */
    namespace id {

        static const std::string joint_pos = "joint-pos";
        static const std::string joint_vel = "joint-vel";
        static const std::string torques = "torques";
        static const std::string forces = "ee-forces_";

        std::string EEForceNodes(uint ee);

    } // namespace id

} // namespace gambol

#endif /* VARIABLES_VARIABLESNAMES_H_ */
