#ifndef VARIABLES_COORDINATES_H_
#define VARIABLES_COORDINATES_H_

/** @brief Indicate dimension in 2D */
enum Dim2D {
    X_ = 0, Y_
};

/** @brief Indicate dimension in 3D */
enum Dim3D {
    X = 0, Y, Z
};

#endif /* VARIABLES_COORDINATES_H_ */
