#ifndef VARIABLES_NODESVARIABLES_H_
#define VARIABLES_NODESVARIABLES_H_

#include <ifopt/variable_set.h>

#include "NodeTimes.h"
#include "PhaseDurations.h"

namespace gambol {

    /**
     * Variables that occur on each collocation node.
     *
     * It is assumed basic interpolation is used between nodes. No splines are included
     * in the variable or their optimization.
     */
    class NodesVariables : public ifopt::VariableSet {
    public:
        using Ptr = std::shared_ptr<NodesVariables>;
        using VectorXd = Eigen::VectorXd;

        /**
         * Semantic information associated with a scalar node value
         *
         * This includes all information except the actual value. This comes from
         * the vector of optimization variables.
         */
        struct NodeValueInfo {
            int id_;    ///< ID of the associated node (0 =< id < number of nodes in spline).
            int dim_;    ///< Dimension (x,y,z or joint id) of that derivative.

            /**
             * Default constructor
             */
            NodeValueInfo() = default;

            /**
             * Useful constructor
             *
             * @param node_id
             * @param node_dim
             */
            NodeValueInfo(int node_id, int node_dim);

            /**
             * Overloaded equality-check operator. Return true when both id and dim are equal.
             *
             * @param right
             * @return
             */
            int operator==(const NodeValueInfo& right) const;
        };

        /**
         * Constructor
         */
        NodesVariables(int n_nodes, int n_dim, const std::string& variable_id,
                       const NodeTimes::Ptr& node_times);

        ~NodesVariables() override = default;

        /**
         * Get info of node affected by one specific optimization variable.
         *
         * Reverse of GetOptIndex().
         *
         * @param opt_idx  The index (=row) of the optimization variable.
         * @return The node value affected by this optimization variable.
         */
        virtual NodeValueInfo GetNodeInfo(int opt_idx) const;

        /**
         * Get index of specific value inside the big stacked opt. vector
         *
         * This function simply overloads the basic and doesn't require
         * a NVI struct.
         */
        virtual int GetOptIndex(const NodeValueInfo& nvi_des) const;

        /**
         * Get opt index of specific value, without the NVI struct
         */
        virtual int GetOptIndex(int node_id, int dim) const;

        /**
         * Pure optimization variables that define the nodes
         */
        VectorXd GetValues() const override;

        /**
         * Sets node positions from the optimization variables.
         * @param x The optimization variables.
         */
        void SetVariables(const VectorXd& x) override;

        /**
         * Set nodes directly
         */
        void SetVariables(const std::vector<VectorXd>& nodes);

        /** Return number of dimensions */
        int GetDim() const;

        /** Return number of nodes */
        int GetNumNodes() const;

        /**
         * @returns the bounds on position and velocity of each node and dimension.
         */
        VecBound GetBounds() const override;

        /**
         * @returns All the nodes.
         */
        const std::vector<VectorXd>& GetNodes() const;

        /**
         * @returns A single node.
         */
        const VectorXd& GetNode(int k) const;

        /**
         * @returns An interpolated point by time
         */
        VectorXd GetPoint(double t) const;

        /**
         * Set specific node value
         */
        void SetNode(int k, const VectorXd& value);

        /**
         * Return jacobian of a node w.r.t. the entire variable stack.
         *
         * Resulting jacobian has `dim` rows and `dim * n_nodes` columns.
         * It will consist of zeros with an identity block.
         *
         * @param k		ID of the node to which the jacobian is for
         */
        Jacobian GetNodeJacobian(int k) const;

        /**
         * @brief Sets nodes pos/vel equally spaced from initial to final position.
         * @param initial_val  value of the first node.
         * @param final_val  value of the final node.
         */
        void SetByLinearInterpolation(const VectorXd& initial_val,
                                      const VectorXd& final_val);

        /**
         * @brief 			Sets node values for contact or flight phase
         * @param value		Constant value to be set
         * @param contact	Which phase to set
         * @param phases	Pointer to phases definition
         */
        void SetConstantByPhase(const VectorXd& value, bool contact, const PhaseDurations::Ptr& phases);

        /**
         * @brief Restricts the first node in the spline.
         * @param dimensions Which dimensions (x,y,z) should be restricted.
         * @param val The values the fist node should be set to.
         */
        void AddStartBound(const std::vector<int>& dimensions, const VectorXd& val);

        /**
         * @brief Restricts the last node in the spline.
         * @param dimensions Which dimensions (x,y,z) should be restricted.
         * @param val The values the last node should be set to.
         */
        void AddFinalBound(const std::vector<int>& dimensions, const VectorXd& val);

        /**
         * Set constant limits to all joints
         *
         * This will overwrite Start and Final bounds!
         *
         * @param dimensions List of dimensions to restrict
         * @param limits The min and max applied to each node
         */
        void AddGlobalBound(const std::vector<int>& dimensions,
                            const std::vector<std::pair<double, double>>& limits);

    protected:

        VecBound bounds_;                ///< The bounds on the node values.
        std::vector<VectorXd> nodes_;    ///< Actual nodes
        int n_dim_;                        ///< Number of values (=dimensions_ per node
        NodeTimes::Ptr node_times_;        ///< Pointer to node timings

    private:

        /**
         * @brief Bounds a specific node variables.
         * @param node_id  The ID of the node to bound.
         * @param dim      The dimension of the node to bound.
         * @param values   The values to set the bounds to.
         */
        void AddBounds(int node_id, const std::vector<int>& dim,
                       const VectorXd& values);

        /**
         * @brief Restricts a specific optimization variables.
         * @param node_info The specs of the optimization variables to restrict.
         * @param value     The value to set the bounds to.
         */
        void AddBound(const NodeValueInfo& node_info, double value);
    };

} /* namespace gambol */

#endif /* VARIABLES_NODESVARIABLES_H_ */
