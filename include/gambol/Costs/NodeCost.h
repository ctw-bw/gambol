#ifndef COSTS_NODECOST_H_
#define COSTS_NODECOST_H_

#include <string>
#include <ifopt/cost_term.h>

#include <gambol/Variables/NodesVariables.h>

namespace gambol {

    /**
     * Cost term based on a node value
     *
     * Cost is the squared sum of node value. One instance of this class
     * governs a single dimension.
     */
    class NodeCost : public ifopt::CostTerm {
    public:

        /**
         * Constructor.
         *
         * @param nodes_id		Name of the linked variable
         * @param dim			Dimension of this variable to use
         * @param weight 		Relative weight of this cost
         * @param exp			2 by default (square)
         * @param prefix		Extra prefix to the name of this object
         */
        NodeCost(const std::string& nodes_id, int dim, double weight,
                 double exp = 2.0, const std::string& prefix = "");

        ~NodeCost() override = default;

        /**
         * Link to optimization variable
         */
        void InitVariableDependedQuantities(const VariablesPtr& x) override;

        /**
         * Calculate cost value
         */
        double GetCost() const override;

    protected:

        /**
         * Calculate section of jacobian
         */
        void FillJacobianBlock(std::string var_set, Jacobian&) const override;

        NodesVariables::Ptr nodes_; ///< Pointer to variable to use as cost

        std::string node_id_; ///< Name of the variable
        int dim_; ///< Dimension of this variable to use
        double weight_;
        double exp_;
    };

} /* namespace gambol */

#endif /* COSTS_NODECOST_H_ */
