![Gambol Logo](gambol.png)

[![Documentation Status](https://readthedocs.org/projects/gambol/badge/?version=latest)](https://gambol.readthedocs.io/en/latest/?badge=latest)
[![CodeFactor](https://www.codefactor.io/repository/bitbucket/ctw-bw/gambol/badge)](https://www.codefactor.io/repository/bitbucket/ctw-bw/gambol)


An optimization tool based on [IFOPT](https://github.com/ethz-adrl/ifopt), focused on [MuJoCo](http://mujoco.org) models.  
It is intended as a counterpart to [TOWR](https://github.com/ethz-adrl/towr).

Source documentation can be found here: <https://gambol.readthedocs.io/>

The report of the corresponding assignment can be found here: https://essay.utwente.nl/85467/

# Install #

## Dependencies ##

The following dependencies (in order) are needed.

* **MuJoCo and MuJoCoTools**

The simulator MuJoCo (<http://www.mujoco.org>) is used as a dynamics back-end. You will need to acquire a license (free for students).  
MuJoCoTools is a C++ wrapper for MuJoCo. Install it from here: <https://bitbucket.org/ctw-bw/mujoco-tools>  
Also consider the MuJoCoTools repository for instructions on how to properly install MuJoCo.

* **RaiSim and RaiSimOgre**

The RaiSim simulator (<https://raisim.com/>) can also be used as a dynamics back-end.
The build system currently does not support a toggle, so you will need to install MuJoCo too.

Get `raisimLib` from its repository: <https://github.com/raisimTech/raisimLib>.
Installation instructions can be found on the website: <https://raisim.com/sections/Installation.html>

`raisimOgre` is used as a visualization tool (<https://github.com/raisimTech/raisimOgre>).
Consult the repo ReadMe on how to install it.  

* **IPOPT**

The non-linear numerical optimization tool [IPOPT](https://coin-or.github.io/Ipopt/) is used. You can either install it using `apt` or build it from source.

Installing directly:

```
 sudo apt install coinor-libipopt-dev
```

Building from source is explained here: https://coin-or.github.io/Ipopt/INSTALL.html

The advantage of building from source is you can use a more advanced solver, like MA27 or MA97. The packaged IPOPT comes with NUMPS.

* **IFOPT**

Install IFOPT according to: https://github.com/ethz-adrl/ifopt/  
You will probably want to use the simple cmake install.

## Build ##

To build Gambol using cmake:

* `mkdir build && cd build`
* `cmake .. -DCMAKE_BUILD_TYPE=Release` ('Release' uses `-Ofast`, which is important)
* `make`

The pipeline file `bitbucket-pipelines.yml` can also be checked as a last resort. The code is automatically being built after each commit, it can be used as an example on how to compile.

# Documentation #

Documentation is built using Sphinx, which in turn call Doxygen and ties
it together with breathe. Build it with:

```shell
cd docs
make html
```

The output will be in `docs/_build/html`.

Bare Doxygen documentation can also be generated with

```shell
cd docs
doxygen Doxyfile.in
```

Unfortunately documentation is not consistent at this time. 
The preference is to put doc blocks in the headers, not source files, such documentation is also easily available after a `make install`.

Docs are automatically built and hosted on Read-the-Docs.

# Testing #

Some trivial unit tests are available. These are run with [Google Test](https://github.com/google/googletest), 
a C++ unit testing framework.

To get to testing, first install GTest (clone the googletest repo install with cmake). 
Then run `make test` in this repository.

# Authors #

* [Robert Roos](https://robert-roos.nl) - Development

Development of TOWR was done by [Alexander W. Winkler](https://www.alex-winkler.com/).
