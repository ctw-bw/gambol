Constraints
===========

Constraints in the NLP are created through the following classes.


Nodes Constraint
----------------

.. doxygenfile:: Constraints/NodesConstraint.h


Dynamics
--------

.. doxygenfile:: Constraints/DynamicsConstraint.h


Integration
-----------

.. doxygenfile:: Constraints/IntegrationConstraint.h


Terrain
-------

.. doxygenfile:: Constraints/TerrainConstraint.h

.. doxygenfile:: Constraints/TerrainFlatConstraint.h


Forces
------

.. doxygenfile:: Constraints/ForceConstraint.h

.. doxygenfile:: Constraints/ForceFlatConstraint.h


Quaternion
----------

.. doxygenfile:: Constraints/QuaternionConstraint.h


Symmetry
--------

.. doxygenfile:: Constraints/SymmetryConstraint.h
