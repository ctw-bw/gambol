#include <gambol/Constraints/IntegrationConstraint.h>
#include <gambol/Variables/VariablesNames.h>

namespace gambol {

    // Constructor
    IntegrationConstraint::IntegrationConstraint(const RobotModel::Ptr& model,
                                                 const NodesHolder& nodes_holder) :
            NodesConstraint(nodes_holder, "integration") {
        model_k_ = model;
        // Rely on copy constructor to create a duplicate
        model_kp1_ = model->clone();

        size_q_ = nodes_holder_.joint_pos_->GetDim();
        size_dq_ = nodes_holder_.joint_vel_->GetDim();

        // Link all adjacent couples, so skip the last node
        SetRows((GetNumberOfNodes() - 1) * size_q_);

        dt_k_ = 0.0;
    }

    // Get row number
    int IntegrationConstraint::GetRow(int k, int dim) const {
        // Only constraints between nodes
        assert(k < GetNumberOfNodes() - 1 && "Index exceeds number of constraints");

        return (k * size_q_) + dim;
    }

    // Update models
    bool IntegrationConstraint::Update(int k) const {
        if (k >= GetNumberOfNodes() - 1) {
            return false;
        }

        model_k_->SetCurrent(nodes_holder_, k);
        model_kp1_->SetCurrent(nodes_holder_, k + 1);

        dt_k_ = nodes_holder_.node_times_->GetDeltaT(k);

        return true;
    }

    // Get constraint value
    void IntegrationConstraint::UpdateConstraintAtNode(int k, VectorXd& g) const {
        if (!Update(k)) {
            return; // Nothing to do
        }

        VectorXd q_k = nodes_holder_.joint_pos_->GetNode(k);
        VectorXd q_kp1 = nodes_holder_.joint_pos_->GetNode(k + 1);

        VectorXd dq_k = model_k_->GetRatesFromVel();
        VectorXd dq_kp1 = model_kp1_->GetRatesFromVel();

        VectorXd error = q_k - q_kp1 + (dq_k + dq_kp1) * 0.5 * dt_k_;

        g.segment(GetRow(k), error.size()) = error;
    }

    // Get bounds value
    void IntegrationConstraint::UpdateBoundsAtNode(int k, VecBound& bounds) const {
        if (k < GetNumberOfNodes() - 1) {
            for (int i = 0; i < size_q_; i++) {
                bounds[GetRow(k, i)] = ifopt::BoundZero;
            }
        }
    }

    // Get jacobian value
    void IntegrationConstraint::UpdateJacobianAtNode(int k, std::string var_set,
                                                     Jacobian& jac) const {
        if (!Update(k)) {
            return; // Nothing to do
        }

        int n = (int)jac.cols(); // Number of columns of jacobian (= size of variable set)
        Jacobian jac_model(size_q_, n);

        auto joint_pos = nodes_holder_.joint_pos_;
        auto joint_vel = nodes_holder_.joint_vel_;

        if (var_set == joint_pos->GetName()) {
            Jacobian node_jac_k = joint_pos->GetNodeJacobian(k);
            Jacobian node_jac_kp1 = joint_pos->GetNodeJacobian(k + 1);

            // q[k] - q[k+1]
            Jacobian jac_position = node_jac_k - node_jac_kp1;
            // 0.5 * dt * (dq[k] + dq[k+1])
            Jacobian jac_velocity = (model_k_->GetRatesJacobianWrtPos()
                                     * node_jac_k
                                     + model_kp1_->GetRatesJacobianWrtPos() * node_jac_kp1) * 0.5
                                    * dt_k_;
            jac_model = jac_position + jac_velocity;
        }

        if (var_set == joint_vel->GetName()) {
            // 0.5 * dt * (dq[k] + dq[k+1])
            jac_model = (model_k_->GetRatesJacobianWrtVel()
                         * joint_vel->GetNodeJacobian(k)
                         + model_kp1_->GetRatesJacobianWrtVel()
                           * joint_vel->GetNodeJacobian(k + 1)) * 0.5 * dt_k_;
        }

        jac.middleRows(GetRow(k), size_q_) = jac_model;
    }

} /* namespace gambol */
