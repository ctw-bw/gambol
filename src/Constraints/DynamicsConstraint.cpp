#include <memory>
#include <gambol/Constraints/DynamicsConstraint.h>

namespace gambol {

    // Constructor
    DynamicsConstraint::DynamicsConstraint(const RobotModel::Ptr& model,
                                           const NodesHolder& s) :
            NodesConstraint(s, "dynamics") {
        model_k_ = model;
        // Rely on copy constructor to create a duplicate
        model_kp1_ = model->clone();

        size_q_ = nodes_holder_.joint_pos_->GetDim();
        size_dq_ = nodes_holder_.joint_vel_->GetDim();
        dt_k_ = 0.0;

        // Link all adjacent couples, so skip the last node
        SetRows((GetNumberOfNodes() - 1) * size_dq_);
    }

    // GetRow
    int DynamicsConstraint::GetRow(int k, int dim) const {
        // Only constraints between nodes
        assert(k < GetNumberOfNodes() - 1 && "Index exceeds number of constraints");

        return (k * size_dq_) + dim;
    }

    // Update
    bool DynamicsConstraint::Update(int k) const {
        if (k >= GetNumberOfNodes() - 1) {
            return false;
        }

        model_k_->SetCurrent(nodes_holder_, k);
        model_kp1_->SetCurrent(nodes_holder_, k + 1);

        dt_k_ = nodes_holder_.node_times_->GetDeltaT(k);

        // TODO: Optimise this sequence such that data is copied instead of recalculated
        return true;
    }

    // Get constraint value
    void DynamicsConstraint::UpdateConstraintAtNode(int k, VectorXd& g) const {
        if (!Update(k)) {
            return; // Nothing to do
        }

        VectorXd dq_k = nodes_holder_.joint_vel_->GetNode(k);
        VectorXd dq_kp1 = nodes_holder_.joint_vel_->GetNode(k + 1);

        VectorXd ddq_k = model_k_->GetDynamics();
        VectorXd ddq_kp1 = model_kp1_->GetDynamics();

        VectorXd error = dq_k - dq_kp1 + (ddq_k + ddq_kp1) * 0.5 * dt_k_;

        g.segment(GetRow(k), error.size()) = error;
    }

    // Get bounds value
    void DynamicsConstraint::UpdateBoundsAtNode(int k, VecBound& bounds) const {
        if (k < GetNumberOfNodes() - 1) {
            for (int i = 0; i < size_dq_; i++) {
                bounds[GetRow(k, i)] = ifopt::BoundZero;
            }
        }
    }

    // Get jacobian value
    void DynamicsConstraint::UpdateJacobianAtNode(int k, std::string var_set,
                                                  Jacobian& jac) const {
        if (!Update(k)) {
            return; // Nothing to do
        }

        int n = (int)jac.cols(); // Number of columns of jacobian (= size of variable set)
        Jacobian jac_model(size_dq_, n);

        auto joint_pos = nodes_holder_.joint_pos_;
        if (var_set == joint_pos->GetName()) {
            Jacobian ddq_diff_qpos_k = model_k_->GetDynamicsJacobianWrtPos();
            Jacobian ddq_diff_qpos_kp1 = model_kp1_->GetDynamicsJacobianWrtPos();

            // ... 0.5 * dt * (ddq[k] + ddq[k+1])
            Jacobian jac_ddq = ddq_diff_qpos_k * joint_pos->GetNodeJacobian(k)
                               + ddq_diff_qpos_kp1 * joint_pos->GetNodeJacobian(k + 1);

            jac_model = jac_ddq * 0.5 * dt_k_;
        }

        auto joint_vel = nodes_holder_.joint_vel_;
        if (var_set == joint_vel->GetName()) {
            Jacobian ddq_diff_qvel_k = model_k_->GetDynamicsJacobianWrtVel();
            Jacobian ddq_diff_qvel_kp1 = model_kp1_->GetDynamicsJacobianWrtVel();

            // ... 0.5 * dt * (ddq[k] + ddq[k+1])
            Jacobian jac_ddq = ddq_diff_qvel_k * joint_vel->GetNodeJacobian(k)
                               + ddq_diff_qvel_kp1 * joint_vel->GetNodeJacobian(k + 1);

            // dq[k] - dq[k+1] + ...
            jac_model = jac_ddq * 0.5 * dt_k_ + joint_vel->GetNodeJacobian(k)
                        - joint_vel->GetNodeJacobian(k + 1);
        }

        auto torques = nodes_holder_.torques_;
        if (var_set == torques->GetName()) {
            Jacobian ddq_diff_torque_k = model_k_->GetDynamicsJacobianWrtTorque();
            Jacobian ddq_diff_torque_kp1 =
                    model_kp1_->GetDynamicsJacobianWrtTorque();

            // ... 0.5 * dt * (ddq[k] + ddq[k+1])
            Jacobian jac_ddq = ddq_diff_torque_k * torques->GetNodeJacobian(k)
                               + ddq_diff_torque_kp1 * torques->GetNodeJacobian(k + 1);

            jac_model = jac_ddq * 0.5 * dt_k_;
        }

        for (int ee = 0; ee < model_k_->GetEECount(); ee++) {
            auto forces = nodes_holder_.ee_forces_[ee];

            if (var_set == forces->GetName()) {
                Jacobian ddq_diff_jac_forces_k =
                        model_k_->GetDynamicsJacobianWrtForces(ee);
                Jacobian ddq_diff_jac_forces_kp1 =
                        model_kp1_->GetDynamicsJacobianWrtForces(ee);

                // ... 0.5 * dt * (ddq[k] + ddq[k+1])
                Jacobian jac_ddq = ddq_diff_jac_forces_k * forces->GetNodeJacobian(k)
                                   + ddq_diff_jac_forces_kp1 * forces->GetNodeJacobian(k + 1);

                jac_model = jac_ddq * 0.5 * dt_k_;
            }
        }

        jac.middleRows(GetRow(k), size_dq_) = jac_model;
    }

} /* namespace gambol */
