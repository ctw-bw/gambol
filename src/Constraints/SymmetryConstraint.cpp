#include <gambol/Constraints/SymmetryConstraint.h>

#include <numeric>

namespace gambol {

    // Constructor
    SymmetryConstraint::SymmetryConstraint(const NodesVariables::Ptr& nodes,
                                           const std::vector<int>& dims) :
            ConstraintSet(kSpecifyLater, "symmetry-" + nodes->GetName()) {
        nodes_ = nodes;

        if (dims.empty()) {
            dims_ = std::vector<int>(nodes_->GetDim());
            std::iota(dims_.begin(), dims_.end(), 0);
        } else {
            dims_ = dims;
        }

        SetRows((int)dims_.size()); // First to last for each dimension
    }

    // Get constraint values
    SymmetryConstraint::VectorXd SymmetryConstraint::GetValues() const {
        VectorXd g = VectorXd(GetRows());

        const int k_end = nodes_->GetNumNodes() - 1;
        int i = 0;
        for (auto dim : dims_) {
            // End minus start
            g[i++] = nodes_->GetNode(k_end)[dim] - nodes_->GetNode(0)[dim];
        }

        return g;
    }

    // Get bounds value
    SymmetryConstraint::VecBound SymmetryConstraint::GetBounds() const {
        VecBound bounds(GetRows());

        for (auto& bound : bounds) {
            bound = ifopt::BoundZero;
        }

        return bounds;
    }

    // Get jacobian section values
    void SymmetryConstraint::FillJacobianBlock(std::string var_set, Jacobian& jac) const {
        if (var_set == nodes_->GetName()) {
            const int k_end = nodes_->GetNumNodes() - 1;

            int i = 0;
            for (auto dim : dims_) {
                int idx_start = nodes_->GetOptIndex(0, dim);
                int idx_end = nodes_->GetOptIndex(k_end, dim);

                jac.coeffRef(i, idx_end) = 1.0;
                jac.coeffRef(i, idx_start) = -1.0;
                i++;
            }
        }
    }

} /* namespace gambol */
