#include <iostream>

#include <gambol/Problem/NlpFormulation.h>

#include <gambol/Variables/VariablesNames.h>

#include <gambol/Constraints/IntegrationConstraint.h>
#include <gambol/Constraints/QuaternionConstraint.h>
#include <gambol/Constraints/DynamicsConstraint.h>
#include <gambol/Constraints/TerrainConstraint.h>
#include <gambol/Constraints/ForceConstraint.h>
#include <gambol/Constraints/SymmetryConstraint.h>

#include <gambol/Costs/NodeCost.h>
#include <gambol/Costs/NodeDerivativeCost.h>
#include <gambol/Costs/FootLiftReward.h>

namespace gambol {

    NlpFormulation::NlpFormulation() {
        using namespace std;
        cout << "\n";
        cout << "*******************************************************\n";
        cout << "                   GAMBOL\n";
        cout << "               \u00a9 Robert Roos\n";
        cout << "*******************************************************";
        cout << "\n\n";

        size_q_ = 0;
        size_dq_ = 0;
        size_u_ = 0;

        initial_zpos_ = false;
    }

    // Set typical solver options
    void NlpFormulation::setSolverOptions(const ifopt::IpoptSolver::Ptr& solver) {
        solver->SetOption("linear_solver", "ma27");
        solver->SetOption("jacobian_approximation", "exact"); // exact / finite-difference-values
        solver->SetOption("print_level", 5);

        solver->SetOption("tol", 1e-4); // Default 1e-8, TOWR default 1e-4

        solver->SetOption("dual_inf_tol", 1e0); // Default 1
        solver->SetOption("constr_viol_tol", 1e-5); // Default 1e-4
        solver->SetOption("compl_inf_tol", 1e-5); // Default 1e-4

        solver->SetOption("acceptable_tol", 1e4); // Default 1e-6
        solver->SetOption("acceptable_iter", 10); // Default 15
        solver->SetOption("acceptable_dual_inf_tol", 1e10); // Default 1e10
        solver->SetOption("acceptable_constr_viol_tol", 1e-3); //Default 1e-2
        solver->SetOption("acceptable_compl_inf_tol", 1e-1); // Default 1e-2
        solver->SetOption("acceptable_obj_change_tol", 1e20); // Default 1e20
    }

    // Get optimization variables
    NlpFormulation::VariablePtrVec NlpFormulation::GetVariableSets(
            NodesHolder& nodes_holder) {
        VariablePtrVec vars;

        node_times_ = MakeNodeTimes();
        phase_durations_ = MakePhaseDurations();

        // Each Make...() method returns a list of variable sets

        auto joint_motion = MakeVariablesJoints();
        vars.insert(vars.end(), joint_motion.begin(), joint_motion.end());

        auto torques = MakeVariablesTorques();
        vars.push_back(torques);

        auto ee_forces = MakeVariablesForces();
        vars.insert(vars.end(), ee_forces.begin(), ee_forces.end());

        // Stores these links to these nodes in solution object
        nodes_holder = NodesHolder(joint_motion.at(0), joint_motion.at(1), torques,
                                   ee_forces, phase_durations_, node_times_);

        // Set initial guess
        if (model_->HasIK()) {
            SetJointsInitialGuess(nodes_holder);
        }

        return vars;
    }

    NodeTimes::Ptr NlpFormulation::MakeNodeTimes() const {
        return std::make_shared<NodeTimes>(params_.t_total, params_.N_);
    }

    std::vector<PhaseDurations::Ptr> NlpFormulation::MakePhaseDurations() const {
        std::vector<PhaseDurations::Ptr> phases;

        for (uint ee = 0; ee < params_.GetEECount(); ee++) {
            auto phase = std::make_shared<PhaseDurations>(
                    params_.ee_phase_durations_[ee], params_.initial_contact_[ee]);

            phases.push_back(phase);
        }

        return phases;
    }

    std::vector<NodesVariables::Ptr> NlpFormulation::MakeVariablesJoints() const {
        int n_nodes = params_.N_;

        // Create position nodes
        auto joint_pos = std::make_shared<NodesVariables>(n_nodes, size_q_,
                                                          id::joint_pos, node_times_);

        joint_pos->SetByLinearInterpolation(initial_joint_pos_, final_joint_pos_);

        joint_pos->AddGlobalBound(bound_joint_pos_limits_, joint_pos_limits_);

        joint_pos->AddStartBound(bound_initial_joint_pos_, initial_joint_pos_);
        joint_pos->AddFinalBound(bound_final_joint_pos_, final_joint_pos_);

        // Create velocity nodes
        auto joint_vel = std::make_shared<NodesVariables>(n_nodes, size_dq_,
                                                          id::joint_vel, node_times_);

        VectorXd avg_rate = (final_joint_pos_ - initial_joint_pos_)
                            / static_cast<double>(n_nodes - 1);

        // The line below doesn't really work since model_ is not updated to the right position
        VectorXd avg_vel = model_->GetVelJacobianWrtRates() * avg_rate;
        joint_vel->SetByLinearInterpolation(avg_vel, avg_vel);

        if (!params_.symmetry_) {
            joint_vel->AddStartBound(bound_initial_joint_vel_, initial_joint_vel_);
            joint_vel->AddFinalBound(bound_final_joint_vel_, final_joint_vel_);
        }

        std::vector<NodesVariables::Ptr> vars = {joint_pos, joint_vel};

        return vars;
    }

    NodesVariables::Ptr NlpFormulation::MakeVariablesTorques() const {
        int n_nodes = params_.N_;

        // Create nodes
        auto torques = std::make_shared<NodesVariables>(n_nodes, size_u_,
                                                        id::torques, node_times_);

        torques->AddGlobalBound(bound_torque_limits_, torque_limits_);

        return torques;
    }

    std::vector<NodesVariables::Ptr> NlpFormulation::MakeVariablesForces() const {
        int n_nodes = params_.N_;

        std::vector<NodesVariables::Ptr> forces;

        // Create nodes
        for (uint ee = 0; ee < params_.GetEECount(); ee++) {
            auto ee_force = std::make_shared<NodesVariables>(n_nodes, 3,
                                                             id::EEForceNodes(ee), node_times_);

            Eigen::VectorXd static_force(3);
            static_force << 0.0, 0.0, (model_->GetTotalMass() * model_->GetGravity());
            //static_force << 1.0, 1.0, 1.0;
            ee_force->SetConstantByPhase(static_force, true, phase_durations_[ee]);

            //ee_force->AddGlobalBound({0, 1, 2}, {{0.0, 0.0}, {0.0, 0.0}, {0.0, 0.0}}); // Disable forces entirely

            forces.push_back(ee_force);
        }

        return forces;
    }

    NlpFormulation::ContraintPtrVec NlpFormulation::GetConstraints(
            const NodesHolder& nodes_holder) const {
        ContraintPtrVec constraints;

        for (auto name : params_.constraints_) {
            for (const auto& c : GetConstraint(name, nodes_holder)) {
                constraints.push_back(c);
            }
        }

        return constraints;
    }

    NlpFormulation::ContraintPtrVec NlpFormulation::GetConstraint(
            Parameters::ConstraintName name, const NodesHolder& s) const {
        switch (name) {
            case Parameters::Integration:
                return MakeConstraintIntegration(s);
            case Parameters::Quaternions:
                return MakeConstraintQuaternions(s);
            case Parameters::Dynamics:
                return MakeConstraintDynamics(s);
            case Parameters::Terrain:
                return MakeConstraintTerrain(s);
            case Parameters::Forces:
                return MakeConstraintForces(s);
            case Parameters::Symmetry:
                return MakeConstraintSymmetry(s);
            default:
                throw std::runtime_error("Formulation: constraint not defined!");
        }
    }

    NlpFormulation::ContraintPtrVec NlpFormulation::MakeConstraintIntegration(
            const NodesHolder& s) const {
        return
                {std::make_shared<IntegrationConstraint>(model_, s)};
    }

    NlpFormulation::ContraintPtrVec NlpFormulation::MakeConstraintQuaternions(
            const NodesHolder& s) const {
        ContraintPtrVec constraints;

        if (!model_->GetQuatIndices().empty()) {
            constraints.push_back(
                    std::make_shared<QuaternionConstraint>(model_, s));
        }

        return constraints;
    }

    NlpFormulation::ContraintPtrVec NlpFormulation::MakeConstraintDynamics(
            const NodesHolder& s) const {
        return
                {std::make_shared<DynamicsConstraint>(model_, s)};
    }

    NlpFormulation::ContraintPtrVec NlpFormulation::MakeConstraintTerrain(
            const NodesHolder& s) const {
        ContraintPtrVec terrain_constraints;

        for (uint ee = 0; ee < params_.GetEECount(); ee++) {
            terrain_constraints.push_back(std::make_shared<TerrainConstraint>(terrain_, model_, s, ee, initial_zpos_));
        }

        return terrain_constraints;
    }

    NlpFormulation::ContraintPtrVec NlpFormulation::MakeConstraintForces(
            const NodesHolder& s) const {
        ContraintPtrVec force_constraints;

        for (uint ee = 0; ee < params_.GetEECount(); ee++) {
            force_constraints.push_back(
                    std::make_shared<ForceConstraint>(terrain_, model_, s, params_.max_normal_force_, ee));
        }

        return force_constraints;
    }

    NlpFormulation::ContraintPtrVec NlpFormulation::MakeConstraintSymmetry(
            const NodesHolder& s) const {
        ContraintPtrVec constraints;

        constraints.push_back(
                std::make_shared<SymmetryConstraint>(s.joint_pos_,
                                                     bound_symmetry_joint_pos_));
        constraints.push_back(std::make_shared<SymmetryConstraint>(s.joint_vel_));
        constraints.push_back(std::make_shared<SymmetryConstraint>(s.torques_));

        return constraints;
    }

    NlpFormulation::ContraintPtrVec NlpFormulation::GetCosts() const {
        ContraintPtrVec costs;

        for (const auto& pair : params_.costs_) {
            for (const auto& c : GetCost(pair.first, pair.second)) {
                costs.push_back(c);
            }
        }

        return costs;
    }

    NlpFormulation::CostPtrVec NlpFormulation::GetCost(
            const Parameters::CostName& name, double weight) const {
        switch (name) {
            case Parameters::Torque:
                return MakeCostTorque(weight);
            case Parameters::JointAcceleration:
                return MakeCostJointAcceleration(weight);
            case Parameters::AngularVelocity:
                return MakeCostAngularVelocity(weight);
            case Parameters::FootLiftReward:
                return MakeCostFootLift(weight);
            default:
                throw std::runtime_error("Formulation: cost not defined!");
        }
    }

    NlpFormulation::CostPtrVec NlpFormulation::MakeCostTorque(double weight) const {
        CostPtrVec cost;

        for (int i = 0; i < size_u_; i++) {
            cost.push_back(std::make_shared<NodeCost>(id::torques, i, weight, 2.0));
        }

        return cost;
    }

    NlpFormulation::CostPtrVec NlpFormulation::MakeCostJointAcceleration(
            double weight) const {
        CostPtrVec cost;

        for (int i = 0; i < size_dq_; i++) {
            cost.push_back(
                    std::make_shared<NodeDerivativeCost>(id::joint_vel, i, weight));
        }

        return cost;
    }

    NlpFormulation::CostPtrVec NlpFormulation::MakeCostAngularVelocity(
            double weight) const {
        CostPtrVec cost;

        auto quat_ids = model_->GetQuatIndices();
        if (quat_ids.empty()) {
            // Third element if base angle
            cost.push_back(std::make_shared<NodeCost>(id::joint_vel, 2, weight));
        } else {
            int q_i = quat_ids.front();
            for (int i = q_i; i < q_i + 3; i++) {
                cost.push_back(
                        std::make_shared<NodeCost>(id::joint_vel, i, weight));
            }
        }

        return cost;
    }

    NlpFormulation::CostPtrVec NlpFormulation::MakeCostFootLift(double weight) const {
        CostPtrVec cost;

        const uint n_ee = params_.GetEECount();

        for (uint ee = 0; ee < n_ee; ee++) {
            cost.push_back(
                    std::make_shared<FootLiftReward>(node_times_, phase_durations_,
                                                     model_, ee, weight));
        }

        return cost;
    }

    void NlpFormulation::SetJointsInitialGuess(NodesHolder& nodes_holder) const {
        int n_nodes = params_.N_;
        uint n_ee = params_.GetEECount();

        model_->SetCurrent(nodes_holder, n_nodes - 1);

        Vector3d base_pos_fin = model_->GetBasePos();
        std::vector<Vector3d> ee_pos_fin;
        for (uint ee = 0; ee < n_ee; ee++) {
            ee_pos_fin.push_back(model_->GetEEPos(ee));
            ee_pos_fin.back()[2] = 0.0; // Set to ground
        }

        model_->SetCurrent(nodes_holder, 0);

        Vector3d base_pos_ini = model_->GetBasePos();
        std::vector<Vector3d> ee_pos_ini;
        for (uint ee = 0; ee < n_ee; ee++) {
            ee_pos_ini.push_back(model_->GetEEPos(ee));
            ee_pos_ini.back()[2] = 0.0; // Set to ground
        }

        // Count free foot nodes
        std::vector<int> ee_free_nodes(n_ee, 1); // Initialize at 1
        for (int k = 0; k < n_nodes; k++) {
            double t = node_times_->at(k);
            for (uint ee = 0; ee < n_ee; ee++) {
                if (!phase_durations_[ee]->IsContactPhase(t)) {
                    ee_free_nodes[ee]++; // Count swing nodes
                }
            }
        }

        Vector3d delta_base_pos = (base_pos_fin - base_pos_ini)
                                  / static_cast<double>(n_nodes - 1);
        std::vector<Vector3d> delta_ee_pos;
        for (uint ee = 0; ee < n_ee; ee++) {
            if (ee_free_nodes[ee] > 1) {
                delta_ee_pos.emplace_back((ee_pos_fin[ee] - ee_pos_ini[ee])
                                          / static_cast<double>(ee_free_nodes[ee] - 1));
            } else {
                delta_ee_pos.emplace_back(Vector3d::Zero());
            }
        }

        Vector3d base_pos = base_pos_ini;
        std::vector<Vector3d> ee_pos = ee_pos_ini;

        VectorXd qpos_prev = nodes_holder.joint_pos_->GetNode(0);
        VectorXd qpos_dot_prev = VectorXd::Zero(size_q_);

        Vector4d base_rot;
        auto quats = model_->GetQuatIndices();
        if (quats.empty()) {
            base_rot << 1.0, 0.0, 0.0, 0.0; // Unity
        } else {
            base_rot = initial_joint_pos_.segment(quats.front(), 4);
        }

        // Step through nodes
        for (int k = 0; k < n_nodes; k++) {
            if (k > 0) {
                double t = node_times_->at(k);

                base_pos += delta_base_pos;

                for (uint ee = 0; ee < n_ee; ee++) {
                    if (!phase_durations_[ee]->IsContactPhase(t)) {
                        ee_pos[ee] += delta_ee_pos[ee];
                    }
                }
            }

            model_->SetCurrent(nodes_holder, k);

            VectorXd qpos = model_->GetInverseKinematics(base_pos, base_rot,
                                                         ee_pos);
            nodes_holder.joint_pos_->SetNode(k, qpos);

            VectorXd qpos_dot;
            VectorXd qvel;
            if (k > 0) {
                double dt = nodes_holder.node_times_->GetDeltaT(k - 1);
                qpos_dot = (qpos - qpos_prev) * (2 / dt) - qpos_dot_prev;
            } else {
                qpos_dot = model_->GetRatesJacobianWrtVel() * initial_joint_vel_;
            }

            qvel = model_->GetVelJacobianWrtRates() * qpos_dot;
            nodes_holder.joint_vel_->SetNode(k, qvel);

            qpos_prev = qpos;
            qpos_dot_prev = qpos_dot;
        }
    }

} // namespace gambol
