#include <vector>
#include <numeric>
#include <cassert>
#include <cmath>

#include <gambol/Problem/Parameters.h>

namespace gambol {

    Parameters::Parameters() {
        N_ = 10;
        t_total = 0.0;

        symmetry_ = false;

        // Constraints
        constraints_.push_back(Integration);
        constraints_.push_back(Quaternions);
        constraints_.push_back(Dynamics);
        constraints_.push_back(Terrain);
        constraints_.push_back(Forces);

        if (symmetry_) {
            constraints_.push_back(Symmetry);
        }

        // Costs
        costs_.push_back(std::make_pair(Torque, 0.1));
        //costs_.push_back(std::make_pair(FootLiftReward, 100.0));
        //costs_.push_back(std::make_pair(AngularVelocity, 100.0));
        //costs_.push_back(std::make_pair(JointAcceleration, 1.0));

        // Gait stuff
        max_normal_force_ = 10000.0;
    }

    double Parameters::GetTotalTime() const {
        std::vector<double> T_feet;

        for (const auto& v : ee_phase_durations_) {
            T_feet.push_back(std::accumulate(v.begin(), v.end(), 0.0));
        }

        // Safety check that all feet durations sum to same value
        double T = T_feet.empty() ? 0.0 : T_feet.front(); // Take first foot as reference
        for (double Tf : T_feet) {
            assert(
                    fabs(Tf - T) < 1e-6
                    && "Total time of phases is not identical for each EE!");
        }

        return T;
    }

    uint Parameters::GetEECount() const {
        return ee_phase_durations_.size();
    }

    std::vector<int> Parameters::GetLinspaceVector(int n, int start, int step) {
        std::vector<int> vec(n);

        for (int& v : vec) {
            v = start;
            start += step;
        }

        return vec;
    }

} /* namespace gambol */
