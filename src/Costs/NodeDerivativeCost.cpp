#include <gambol/Costs/NodeDerivativeCost.h>
#include <iostream>

namespace gambol {

    // Constructor
    NodeDerivativeCost::NodeDerivativeCost(const std::string& nodes_id, int dim,
                                           double weight) :
            NodeCost(nodes_id, dim, weight, 2.0, "diff-") {
        // Empty
    }

    // Calculate the cost
    double NodeDerivativeCost::GetCost() const {
        double cost = 0.0;
        for (int k = 1; k < nodes_->GetNumNodes(); k++) {
            double val = nodes_->GetNode(k)[dim_] - nodes_->GetNode(k - 1)[dim_];
            cost += weight_ * std::pow(val, 2.0); // Square
        }

        return cost;
    }

    // Calculate part of jacobian
    void NodeDerivativeCost::FillJacobianBlock(std::string var_set,
                                               Jacobian& jac) const {
        if (var_set == node_id_) {
            for (int k = 0; k < nodes_->GetNumNodes(); k++) {
                double val = 0.0;

                if (k == 0) {
                    // If first
                    val = 2.0 * nodes_->GetNode(k)[dim_]
                          - 2.0 * nodes_->GetNode(k + 1)[dim_];
                } else if (k == nodes_->GetNumNodes() - 1) {
                    // If last
                    val = 2.0 * nodes_->GetNode(k)[dim_]
                          - 2.0 * nodes_->GetNode(k - 1)[dim_];
                } else {
                    val = -2.0 * nodes_->GetNode(k + 1)[dim_]
                          + 4.0 * nodes_->GetNode(k)[dim_]
                          - 2.0 * nodes_->GetNode(k - 1)[dim_];
                }

                int idx = nodes_->GetOptIndex(k, dim_);
                jac.coeffRef(0, idx) = weight_ * val;
            }
        }
    }

} /* namespace gambol */
