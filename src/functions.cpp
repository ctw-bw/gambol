#include <gambol/main.h>

#include <iostream>
#include <iomanip>

using namespace gambol;

/**
 * Print nodes of solution
 */
void printSolution(const NodesHolder& solution) {
    using namespace std;

    cout << setprecision(5) << fixed << left;

    const int w = 8;

    cout << left;

    cout << setw(w * 2) << "t";
    cout << setw(w + w + 1 + w * solution.joint_pos_->GetNode(0).size())
         << "pos";
    cout << setw(w + w + w * solution.joint_vel_->GetNode(0).size()) << "vel";
    cout << setw(w + 2 + w * solution.torques_->GetNode(0).size()) << "torque";
    cout << setw(w + w + w * 3) << "force" << endl;

    for (int k = 0; k < solution.node_times_->GetNumberOfNodes(); k++) {
        cout << setw(w) << solution.node_times_->at(k);
        cout << setw(w) << solution.joint_pos_->GetNode(k).transpose();
        cout << setw(w) << solution.joint_vel_->GetNode(k).transpose();
        cout << setw(w) << solution.torques_->GetNode(k).transpose();
        for (const auto& force : solution.ee_forces_)
            cout << setw(w) << force->GetNode(k).transpose();
        cout << endl;
    }
}

/**
 * Print violated constraints
 */
void printConstraintViolations(ifopt::Problem& nlp) {
    using namespace std;

    cout << endl << "Constraint violations:" << endl;

    const Eigen::VectorXd values = nlp.GetVariableValues();
    const Eigen::VectorXd constr_values = nlp.EvaluateConstraints(
            values.data());
    auto bounds = nlp.GetBoundsOnConstraints();

    cout.precision(5);
    cout << scientific;
    for (int i = 0; i < constr_values.size(); i++) {
        const double eps = 1e-4;
        if (constr_values[i] < bounds[i].lower_ - eps
            || constr_values[i] > bounds[i].upper_ + eps) {
            cout << i << ": " << constr_values[i];
            cout << " <" << bounds[i].lower_ << "," << bounds[i].upper_ << ">";
            cout << endl;
        }
    }
}

/**
 * Print and show the contact schedule
 */
void printContactSchedule(const NodesHolder& solution) {
    using namespace std;
    cout << endl << "Contact schedule:" << endl;

    double total_time = solution.node_times_->GetTotalTime();

    int ee = 0;
    for (const auto& durations : solution.phase_durations_) {
        cout << "ee " << ee << ": ";

        for (auto dur : durations->GetPhaseDurations()) {
            cout << dur << ", ";
        }
        cout << endl;

        for (double t = 0; t <= total_time; t += 0.02) {
            if (durations->IsContactPhase(t)) {
                cout << "x";
            } else {
                cout << " ";
            }
        }
        cout << endl;
        ee++;
    }
}

