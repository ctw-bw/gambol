#include <gambol/main.h>

#if MODE_SIMULATE

#include <memory>
#include <iostream>
#include <chrono>
#include <thread>
#include <mutex>

#include <Eigen/Dense>

#include <MuJoCoTools/MuJoCoViewer.h>
#include <gambol/Variables/NodesHolder.h>
#include <gambol/Robots/Robot.h>
#include <gambol/Robots/MuJoCoRobotModel.h>
#include <gambol/Terrain/HeightMap.h>
#include <gambol/Generators/GaitGenerator.h>
#include <gambol/Control/MPC.h>
#include <gambol/Control/CsvLogger.h>
#include <gambol/Control/RobotState.h>

using namespace gambol;

void update_viewer(const mjModel* m, mjData* d);

/**
 * Set to `true` to simulate solution instead of animating it
 */
#define SIMULATE_PHYSICS true

/**
 * Set to `true` to use real MuJoCo contacts
 * When `false`, the predicted forces are applied directly
 */
#define TRUE_CONTACT true

std::shared_ptr<MuJoCoViewer> viewer = nullptr;
std::mutex mux_viewer;

int main()
{

    /**
     * Interval after which MPC is re-calculated
     */
    const double delta_t_mpc = 0.1;

    /**
     * Controller settings
     */
    const double k_p = 20.0; // Position feedback gain (170)
    const double k_d = 2.0; // Velocity feedback gain (10)
    const double k_i = 20.0; // Position integral feedback
    const double k_ff = 1.0; // Feedforward gain

    const double u_fb_max = 20.0; // Maximum allowed feedback torque (cap)

    /**
     * Prepare MPC
     */

    MPC mpc;

    auto formulation = &mpc.formulation_; // Create pointers for convenience
    auto params = &formulation->params_;

    // Select robot
    Robot robot(Robot::BipedFeet3D);

    // Select terrain
    formulation->terrain_ = HeightMap::MakeTerrain(HeightMap::FlatID);

    formulation->model_ = robot.robot_model_;
    params->N_ = 25;
    formulation->size_q_ = robot.size_q_;
    formulation->size_dq_ = robot.size_dq_;
    formulation->size_u_ = robot.size_u_;

    // Gait generator

    auto generator = robot.generator_;
    using GG = GaitGenerator;
    //generator->SetCombo(GG::C0);
    std::vector<GG::Gaits> gaits = { GG::Stand, GG::Walk1E, GG::Stand };
    generator->SetGaits(gaits);

    int steps = generator->GetSteps();
    double step_distance = 0.4;
    double step_time = 1.0;
    double total_duration = step_time
            * static_cast<double>(steps > 0 ? steps : 1);
    double distance = step_distance * static_cast<double>(steps);

    params->ee_phase_durations_ = generator->GetPhaseDurations(total_duration);
    params->initial_contact_ = generator->IsInContactAtStart();
    params->t_total = params->GetTotalTime();

    // Initial / final config
    formulation->initial_joint_vel_ = formulation->final_joint_vel_ =
            Eigen::VectorXd::Zero(robot.size_dq_);
    formulation->initial_joint_pos_ = robot.basic_joint_pos_;
    formulation->final_joint_pos_ = robot.basic_joint_pos_;
    formulation->final_joint_pos_[0] += distance;
    //formulation->final_joint_pos_[1] += 0.707 * distance; // 3D motion

    formulation->bound_initial_joint_pos_ = robot.bound_initial_joint_pos_;
    formulation->bound_final_joint_pos_ = formulation->bound_initial_joint_pos_;
    formulation->bound_initial_joint_vel_ = robot.bound_initial_joint_vel_;
    formulation->bound_final_joint_vel_ = formulation->bound_initial_joint_vel_;

    formulation->initial_zpos_ = false; // Use initial z-position as constraint

    formulation->bound_symmetry_joint_pos_ = robot.bound_symmetry_joint_pos_;

    // Joint and torque limits
    formulation->joint_pos_limits_ = robot.joint_pos_limits_;
    formulation->bound_joint_pos_limits_ = robot.bound_joint_pos_limits_;

    formulation->torque_limits_ = robot.torque_limits_;
    formulation->bound_torque_limits_ = robot.bound_torque_limits_;

    /**
     * Run simulation
     */

    const auto mj_robot = std::dynamic_pointer_cast<MuJoCoRobotModel>(
            formulation->model_);

    const std::string file = mj_robot->GetMuJoCoModel().get_file();

    MuJoCoModel model(file);

    const mjModel* m = model.get_model();
    mjData* d = model.get_data();

    // Make terrain
    Eigen::MatrixXd map = model.get_heightfield(0, true);
    const double r_x = 5.0, r_y = 5.0;
    for (int r = 0; r < map.rows(); r++)
    {
        for (int c = 0; c < map.cols(); c++)
        {
            const double y = ((2.0 * static_cast<double>(r)
                    / static_cast<double>(map.rows() - 1)) - 1.0) * r_x;
            const double x = ((2.0 * static_cast<double>(c)
                    / static_cast<double>(map.cols() - 1)) - 1.0) * r_y;

            map(r, c) = formulation->terrain_->GetHeight(x, y);
        }
    }
    if (!model.set_terrain(0, map, r_x, r_y, 0.5))
    {
        std::cout << std::endl << "Failed to create terrain!" << std::endl
                << std::endl;
    }

#if !TRUE_CONTACT
    model.enable_contact(false); // Disable contact
#endif

    // Launch thread to update viewer asynchronously
    std::thread t_viewer(update_viewer, m, d);

    while (!viewer)
    {
        // Hold until creation finished
        MuJoCoViewer::sleep_seconds(0.1);
    }

    MuJoCoFigure fig_ff("Feedforward", m->nu);
    MuJoCoFigure fig_fb("Feedback", m->nu);
    const int width = 400, height = 200;
    fig_ff.set_size(width, height);
    fig_fb.set_size(width, height);

    mux_viewer.lock();

    viewer->add_figure(&fig_ff);
    viewer->add_figure(&fig_fb);

    mux_viewer.unlock();

    // Move camera
    viewer->cam_.distance = 4.0;
    //viewer.cam.lookat[2] += 0.5;viewer.cam.azimuth = 135.0;viewer.cam.elevation = -20.0;

    bool done = false;

    const double opt_time = params->t_total; // Original optimization time

    mj_resetData(m, d); // Reset data to make sure

    CsvLogger logger(CsvLogger::fileNameFromPath(model.get_file()));

    // List of states per render frame
    std::vector<RobotState> states;
    unsigned int loops = 0; // Total number of animation loops

    // Repeat simulation
    while (!done)
    {
        mj_resetData(m, d); // Reset data to make sure

        formulation->initial_joint_pos_ = robot.basic_joint_pos_;
        formulation->initial_joint_vel_ = Eigen::VectorXd::Zero(robot.size_dq_);

        Eigen::VectorXd qpos0 = formulation->initial_joint_pos_;
        Eigen::VectorXd qvel0 = formulation->initial_joint_vel_;
        model.reset_data(qpos0, qvel0);

        const Eigen::MatrixXd jac_a_transpose =
                model.get_actuation_jacobian().transpose();

        fig_ff.clear();
        fig_fb.clear();

        double time_last_mpc_update = -1.0;

        NodesHolder solution;

        double d_time_prev = 0.0;

        unsigned int frame = 0; // Number of frame into simulation

        // Keep reference between frames for animation
        Eigen::VectorXd qpos_ref(m->nq);
        Eigen::VectorXd qvel_ref(m->nv);

        Eigen::VectorXd qpos_int_error = Eigen::VectorXd::Zero(m->nv); // Position error but in velocity coordinates

        // Run through optimization
        while (d->time < opt_time + 0.0)
        {
            // Record cpu time at start of iteration
            double framestart = glfwGetTime();

            Eigen::VectorXd u_ff(m->nu);
            Eigen::VectorXd u_fb(m->nu);

            if (loops == 0) // Predict and simulate
            {
                if (d->time < opt_time)
                {
                    // Check if MPC should be updated yet
                    if ((time_last_mpc_update < 0.0
                            || (d->time - time_last_mpc_update) >= delta_t_mpc))
                    //&& d->time < opt_time - 0.2)
                    {
                        time_last_mpc_update = d->time;

                        Eigen::VectorXd qpos = MuJoCo::mujoco_to_eigen(d->qpos,
                                m->nq);
                        Eigen::VectorXd qvel = MuJoCo::mujoco_to_eigen(d->qvel,
                                m->nv);

                        mpc.SetInitialConfig(qpos, qvel);
                        mpc.SetTime(d->time);

                        //auto old_solution = solution;

                        std::cout << std::endl << "Updating MPC..."
                                << std::endl;
                        bool success = mpc.Predict(solution);

                        if (!success)
                        {
                            //printConstraintViolations(mpc.nlp_);
                            //mpc.nlp_.PrintCurrent();
                            //solution = old_solution; // Use previous instead
                        }

                        // Save predicted trajectory
                        CsvLogger predict_log(
                                CsvLogger::fileNameFromPath(model.get_file())
                                        + '_' + std::to_string(d->time));

                        for (int k = 0;
                                k < solution.node_times_->GetNumberOfNodes();
                                k++)
                        {
                            const double t_k = d->time
                                    + solution.node_times_->at(k);
                            Eigen::VectorXd qpos_k =
                                    solution.joint_pos_->GetNode(k);
                            Eigen::VectorXd qvel_k =
                                    solution.joint_vel_->GetNode(k);
                            Eigen::VectorXd qacc_k = Eigen::VectorXd::Zero(
                                    qvel_k.size());
                            Eigen::VectorXd u_ff_k =
                                    solution.torques_->GetPoint(k);
                            Eigen::VectorXd u_fb_k = Eigen::VectorXd::Zero(
                                    u_ff_k.size());

                            std::vector<Eigen::VectorXd> forces_k;
                            for (auto f : solution.ee_forces_)
                            {
                                forces_k.push_back(f->GetNode(k));
                            }

                            predict_log.log(t_k, qpos_k, qvel_k, qacc_k, qpos_k,
                                    qvel_k, u_ff, u_fb, forces_k, forces_k,
                                    mpc.optimizations_);
                        }

                        std::cout << std::endl
                                << (success ?
                                        "Optimization Successful!" :
                                        "*********** Optimization Failed ***********")
                                << std::endl;

                        //printContactSchedule(solution);
                        //printSolution(solution);

                        // Reset frame time because of this delay
                        framestart = glfwGetTime();
                    }
                }

                Eigen::VectorXd qpos;
                Eigen::VectorXd qvel;
                Eigen::VectorXd qacc;

                double d_time_prev_step = d->time;

                // Advance simulation steps
                while ((d->time - d_time_prev + 1e-6) < 1.0 / 60.0)
                {
                    // MPC takes the last update as t = 0

                    double mpc_time = d->time - time_last_mpc_update;

                    u_ff = solution.torques_->GetPoint(mpc_time) * k_ff;

                    qpos_ref = solution.joint_pos_->GetPoint(mpc_time);
                    qvel_ref = solution.joint_vel_->GetPoint(mpc_time);
                    Eigen::VectorXd qpos_error(m->nv); // Dimension nv, not nq!
                    mj_differentiatePos(m, qpos_error.data(), 1.0, d->qpos,
                            qpos_ref.data());
                    Eigen::VectorXd qvel = MuJoCo::mujoco_to_eigen(d->qvel,
                            m->nv);
                    Eigen::VectorXd qvel_error = qvel_ref - qvel;

                    qpos_int_error += qpos_error * (d->time - d_time_prev_step);

                    d_time_prev_step = d->time;

                    u_fb = jac_a_transpose
                            * (qpos_error * k_p + qvel_error * k_d + qpos_int_error * k_i);

                    for (int i = 0; i < u_fb.size(); i++)
                    {
                        if (u_fb[i] > u_fb_max)
                            u_fb[i] = u_fb_max;
                        else if (u_fb[i] < -u_fb_max)
                            u_fb[i] = -u_fb_max;
                    }

                    MuJoCo::eigen_copy(d->ctrl, u_ff + u_fb);

#if !TRUE_CONTACT
                    // Apply optimized reaction forces directly
                    Eigen::VectorXd xfrc_full(m->nbody * 6);
                    xfrc_full.setZero();

                    auto body_ids = mj_robot->GetEEBodyIds();

                    for (uint ee = 0; ee < solution.ee_forces_.size(); ee++)
                    {
                        int body_id = body_ids.at(ee);
                        auto pos = model.get_position(body_id);
                        if (pos[2] < 0.01) // Only if close to the ground
                        {
                            auto f = solution.ee_forces_.at(ee)->GetPoint(mpc_time);
                            xfrc_full.segment(body_id * 6, 3) = f;
                        }
                    }

                    MuJoCo::eigen_copy(d->xfrc_applied, xfrc_full);
#endif

#if SIMULATE_PHYSICS
                    mj_step(m, d);
#else
                    model.reset_data(qpos_ref, qvel_ref);
                    d->time += 1.0 / 300.0;
#endif

                    // Create log entry
                    qpos = MuJoCo::mujoco_to_eigen(d->qpos, m->nq);
                    qvel = MuJoCo::mujoco_to_eigen(d->qvel, m->nv);
                    qacc = MuJoCo::mujoco_to_eigen(d->qacc, m->nv);

                    std::vector<Eigen::VectorXd> forces_ref;
                    for (auto f : solution.ee_forces_)
                    {
                        forces_ref.push_back(f->GetPoint(mpc_time));
                    }

                    std::vector<Eigen::VectorXd> forces;
                    for (int body_id : mj_robot->GetEEBodyIds())
                    {
                        forces.push_back(model.get_contact_force(body_id));
                    }

                    logger.log(d->time, qpos, qvel, qacc, qpos_ref, qvel_ref,
                            u_ff, u_fb, forces, forces_ref, mpc.optimizations_);
                }

                RobotState state = RobotState(qpos, qvel, u_ff, u_fb);
                states.push_back(state);
            }
            else // Play back recording
            {
                if (frame < states.size())
                {
                    RobotState* state = &states[frame];
                    model.reset_data(state->qpos, state->qvel);
                    u_ff = state->u_ff;
                    u_fb = state->u_fb;
                }
                d->time += 1.0 / 60.0;
            }
            fig_ff.add_values(d->time, u_ff.data());
            fig_fb.add_values(d->time, u_fb.data());

            d_time_prev = d->time; // Because of frame freeze we cannot rely on system time

            frame++;

            // Wait until a total of 1 frame time has passed
            while (glfwGetTime() - framestart < 1.0 / 60.0)
            {
                // Sleep for 0.1 millisecond to prevent a busy wait
                MuJoCoViewer::sleep_seconds(0.1 / 1000.0);
            }

            if (viewer->window_should_close())
            {
                done = true;
                break;
            }
        }
        loops++; // Play again
    }

    // Wait for thread to finish (not really necessary in this case)
    t_viewer.join();

    return 1;

}

/**
 * Update MuJoCo viewer
 *
 * Function is designed to run from a thread.
 * It will only return when the window was closed.
 */
void update_viewer(const mjModel* m, mjData* d)
{
    mux_viewer.lock();
    viewer = std::make_shared<MuJoCoViewer>(m); // Viewer needs to be created inside thread
    mux_viewer.unlock();

    while (!viewer->window_should_close())
    {
        mux_viewer.lock(); // Block until available

        viewer->update(d);

        mux_viewer.unlock();

        MuJoCoViewer::sleep_seconds(1.0 / 60.0);
    }
}

#endif
