#include <cassert>
#include <memory>
#include <gambol/Robots/RaiSimRobotModel.h>

namespace gambol {

    bool RaiSimRobotModel::activated = false;

    RaiSimRobotModel::RaiSimRobotModel(const std::string& urdf_file, int size_q,
                                       int size_dq, int size_u,
                                       const std::vector<std::string>& ee_names,
                                       const MatrixXd& S) :
            RobotModel(size_q, size_dq, size_u, ee_names.size()) {

        if (!RaiSimRobotModel::activated) {
            // By default the activation key is searched in ~/.raisim/activation.raisim,
            // which is actually very nice. So leave it at that.
            //raisim::World::setActivationKey("/usr/local/lib/raisim/activation.raisim");
            RaiSimRobotModel::activated = true;
        }

        // Initialize RaiSim world and system

        ground_ = world_.addGround(0.0);

        world_.setTimeStep(0.005);

        file_ = urdf_file;
        system_ = world_.addArticulatedSystem(file_);

        assert(system_->getGeneralizedCoordinateDim() == (size_t) size_q);
        assert(system_->getGeneralizedVelocityDim() == (size_t) size_dq);

        // Find end-effectors

        ee_names_ = ee_names;
        for (const auto& ee_name : ee_names_) {
            const auto& frame = system_->getFrameByName(ee_name); // This will throw an error when incorrect
            const int frame_id = system_->getFrameIdxByName(ee_name); // This will throw an error when incorrect
            ee_frames_.push_back(&frame);
            ee_frame_ids_.push_back(frame_id);
        }

        system_ee_forces_ = std::vector<VectorXd>(ee_count_, VectorXd::Zero(3)); // Allocate;

        // Set up actuators

        SetSelectionMatrix(S);

        // Find quaternions in coordinates

        for (size_t i = 0; i < system_->getNumberOfJoints(); i++) {
            raisim::ArticulatedSystem::JointRef joint(i, system_);
            if (joint.getType() == raisim::Joint::SPHERICAL) {
                size_t q_id = joint.getIdxInGeneralizedCoordinate();
                quat_ids_.push_back(q_id);
            } else if (joint.getType() == raisim::Joint::FLOATING) {
                size_t q_id = joint.getIdxInGeneralizedCoordinate() + 3;
                quat_ids_.push_back(q_id);
            }
        }


        // Disable collision detection so we are relying on optimized forces
        EnableContact(false);
    }

    RobotModel::Ptr RaiSimRobotModel::clone() const {
        auto model_clone = std::make_shared<RaiSimRobotModel>(
                file_, size_q_, size_dq_, size_u_, ee_names_, actuator_selector_
        );

        // Copy gravity in case it was changed
        model_clone->GetWorld().setGravity(world_.getGravity());

        return model_clone;
    }

    void RaiSimRobotModel::SetSelectionMatrix(MatrixXd S) {

        if (S.size() == 0) { // Default argument passed

            // Assume that that the last `size_u_` coordinates are actuated
            S = MatrixXd(size_dq_, size_u_);
            S << MatrixXd::Zero(size_dq_ - size_u_, size_u_),
                    MatrixXd::Identity(size_u_, size_u_);
        }

        assert(S.cols() == size_u_ && "Columns in selection matrix does not match with number of actuators");
        assert(S.rows() == size_dq_ && "Rows in selection matrix does not match with DOF");

        actuator_selector_ = S;
    }

    Eigen::VectorXd RaiSimRobotModel::GetDynamics() const {

        // Get current state (do not rely on q_, dq_, etc. for versatility)
        const VectorXd q = system_->getGeneralizedCoordinate().e();
        const VectorXd dq = system_->getGeneralizedVelocity().e();

        double t_prev = world_.getWorldTime();

        // Forces applied by setExternalForce() are not persistent, they are cleared on each integration. So apply
        // them every time to make them persistent. We must take care not to apply the forces multiple times, because
        // they will stack!
        SetSystemEEForces(system_ee_forces_);

        world_.integrate();

        VectorXd dq_future = system_->getGeneralizedVelocity().e();

        VectorXd ddq_calculated = (dq_future - dq) / world_.getTimeStep();

        // Restore previous state

        system_->setGeneralizedCoordinate(q);
        system_->setGeneralizedVelocity(dq);

        world_.setWorldTime(t_prev);

        // The contact solver can have updated itself based on the outcome.
        // To ensure this method hasn't changed anything, restore the solver order.
        world_.getContactSolver().setOrder(true);

        return ddq_calculated;
    }

    RaiSimRobotModel::Jacobian RaiSimRobotModel::GetDynamicsJacobianWrtPos() const {

        const VectorXd ddq_original = GetDynamics();

        VectorXd q = q_;

        MatrixXd jac(size_dq_, size_q_);

        // Build matrix column-by-column
        for (int j = 0; j < size_q_; j++) {
            q[j] += eps; // Perturbation
            system_->setGeneralizedCoordinate(q);

            // Quaternions will be perturbed and become non-normal, but Raisim automatically normalizes them again

            VectorXd ddq_perturbed = GetDynamics(); // < Only relies on states inside system_

            jac.col(j) = (ddq_perturbed - ddq_original) / eps;

            q[j] = q_[j]; // Undo perturbation
        }

        system_->setGeneralizedCoordinate(q_);

        return DenseToSparse(jac);
    }

    RaiSimRobotModel::Jacobian RaiSimRobotModel::GetDynamicsJacobianWrtVel() const {

        const VectorXd ddq_original = GetDynamics();

        VectorXd dq = dq_;

        MatrixXd jac(size_dq_, size_dq_);

        // Build matrix column-by-column
        for (int j = 0; j < size_dq_; j++) {
            dq[j] += eps; // Perturbation
            system_->setGeneralizedVelocity(dq);

            VectorXd ddq_perturbed = GetDynamics(); // < Only relies on states inside system_

            jac.col(j) = (ddq_perturbed - ddq_original) / eps;

            dq[j] = dq_[j]; // Undo perturbation
        }

        system_->setGeneralizedVelocity(dq_);

        return DenseToSparse(jac);
    }

    RaiSimRobotModel::Jacobian RaiSimRobotModel::GetDynamicsJacobianWrtTorque() const {

        const VectorXd ddq_original = GetDynamics();

        VectorXd u = u_;

        MatrixXd jac(size_dq_, size_u_);

        // Build matrix column-by-column
        for (int j = 0; j < size_u_; j++) {
            u[j] += eps; // Perturbation

            SetSystemActuatorTorque(u);

            VectorXd ddq_perturbed = GetDynamics(); // < Only relies on states inside system_

            jac.col(j) = (ddq_perturbed - ddq_original) / eps;

            u[j] = u_[j]; // Undo perturbation
        }

        SetSystemActuatorTorque(u_);

        return DenseToSparse(jac);
    }

    RaiSimRobotModel::Jacobian RaiSimRobotModel::GetDynamicsJacobianWrtForces(uint ee_id) const {

        const VectorXd ddq_original = GetDynamics();

        // Assume system_ee_forces_ is already up-to-date with ee_forces_

        MatrixXd jac(size_dq_, 3); // Jacobian w.r.t. a single 3D force

        // Build matrix column-by-column
        for (int j = 0; j < 3; j++) {
            system_ee_forces_[ee_id][j] += eps; // Perturbation

            VectorXd ddq_perturbed = GetDynamics(); // < Only relies on states inside system_

            jac.col(j) = (ddq_perturbed - ddq_original) / eps;

            system_ee_forces_[ee_id][j] = ee_forces_[ee_id][j]; // Undo perturbation
        }

        return DenseToSparse(jac);
    }

    RaiSimRobotModel::Vector3d RaiSimRobotModel::GetEEPos(uint ee_id) const {

        int frame_id = ee_frame_ids_.at(ee_id);

        raisim::Vec<3> pos;
        system_->getFramePosition(frame_id, pos);

        return DenseToSparse(pos.e());
    }

    RaiSimRobotModel::Jacobian RaiSimRobotModel::GetEEPosJacobian(uint ee_id) const {

        int frame_id = ee_frame_ids_[ee_id];

        // This gets: p_dot = J_v(q) * v
        MatrixXd jac_v = MatrixXd::Zero(3, size_dq_);
        system_->getDenseFrameJacobian(frame_id, jac_v);

        // But we need: p_dot = J_q(q) * q_dot

        // J_q(q) = J_v(q) * B^-1(q)
        return DenseToSparse(jac_v) * GetVelJacobianWrtRates();
    }

    void RaiSimRobotModel::Update() {
        system_->setGeneralizedCoordinate(q_);
        system_->setGeneralizedVelocity(dq_);

        SetSystemActuatorTorque(u_); // System torque is persistent

        system_ee_forces_ = ee_forces_; // External forces are not persistent, so keep it in a separate list.
    }

    Eigen::MatrixXd RaiSimRobotModel::QuaternionMatrix(const Vector4d& q) const {
        MatrixXd quat_M(4, 3);

        // "E^T" in https://arxiv.org/pdf/0811.2889.pdf
        quat_M << -q[1], -q[2], -q[3], // row 1
                q[0], q[3], -q[2], // row 2
                -q[3], q[0], q[1], // row 3
                q[2], -q[1], q[0]; // row 4

        return quat_M;
    }

    Eigen::MatrixXd RaiSimRobotModel::AngularVelocityMatrix(Vector3d w) const {
        MatrixXd omega_M(4, 4);

        omega_M << 0, -w[0], -w[1], -w[2], // row 1
                w[0], 0, -w[2], w[1], // row 2
                w[1], w[2], 0, -w[0], // row 3
                w[2], -w[1], w[0], 0; // row 4

        return omega_M;
    }

    const raisim::CoordinateFrame& RaiSimRobotModel::GetEEFrame(uint ee_id) const {
        return *ee_frames_.at(ee_id); // Throw error if invalid
    }

    void RaiSimRobotModel::SetSystemActuatorTorque(const VectorXd& u) const {
        VectorXd tau = actuator_selector_ * u;
        system_->setGeneralizedForce(tau);
    }

    void RaiSimRobotModel::SetSystemEEForces(const std::vector<VectorXd>& forces) const {

        for (unsigned int ee = 0; ee < forces.size(); ee++) {
            const auto& frame = GetEEFrame(ee);
            const auto& force = forces[ee];
            system_->setExternalForce(frame.parentId, raisim::ArticulatedSystem::WORLD_FRAME, force,
                                      raisim::ArticulatedSystem::BODY_FRAME, frame.position);
        }
    }

    void RaiSimRobotModel::EnableContact(bool enable) {

        for (auto& colBody : system_->getCollisionBodies()) {

            // Either let it collide with everything (-1), or with nothing (0)
            if (enable) {
                colBody.setCollisionMask(-1);
            } else {
                colBody.setCollisionMask(0);
            }
        }
    }

    raisim::World& RaiSimRobotModel::GetWorld() {
        return world_;
    }

    raisim::ArticulatedSystem* RaiSimRobotModel::GetSystem() {
        return system_;
    }

    raisim::Ground* RaiSimRobotModel::GetGround() {
        return ground_;
    }

    // Read joint limits
    std::vector<std::pair<double, double>> RaiSimRobotModel::GetJointLimits(bool is_3d, std::vector<int>* bounds) {

        std::vector<std::pair<double, double>> limits(size_q_);

        const auto& sys_limits = system_->getJointLimits();

        const double abs_min = -1.0e19, abs_max = 1.0e19;

        int i_offset = (int) is_3d; // offset = i_q - i_v

        for (int i_q = 0; i_q < size_q_; i_q++) {

            auto& lim = limits[i_q];

            if (is_3d && i_q < 7) {
                lim.first = abs_min;
                lim.second = abs_max;
            } else {
                lim.first = sys_limits[i_q - i_offset][0];
                lim.second = sys_limits[i_q - i_offset][1];
            }

            if (bounds) {
                if (lim.first > abs_min || lim.second < abs_max) {
                    bounds->push_back(i_q);
                }
            }
        }

        return limits;
    }

    // Read torque limits
    std::vector<std::pair<double, double>> RaiSimRobotModel::GetActuatorLimits(std::vector<int>* bounds) {

        std::vector<std::pair<double, double>> limits(size_u_);

        const auto& upper_limits = system_->getActuationUpperLimits();
        const auto& lower_limits = system_->getActuationLowerLimits();

        const double abs_min = -1.0e19, abs_max = 1.0e19;

        const int offset = upper_limits.size() - size_u_;

        for (int i_u = 0; i_u < size_u_; i_u++) {
            auto& lim = limits[i_u];

            lim.first = lower_limits[i_u + offset];
            lim.second = upper_limits[i_u + offset];

            if (bounds) {
                if (lim.first > abs_min || lim.second < abs_max) {
                    bounds->push_back(i_u);
                }
            }
        }

        return limits;
    }

} /* namespace gambol */
