#include <gambol/Terrain/HeightMap.h>
#include <gambol/Terrain/HeightMapExamples.h>
#include <gambol/Robots/RobotModel.h> // Needed for DenseToSparse

#include <cmath>

namespace gambol {

    HeightMap::Ptr HeightMap::MakeTerrain(TerrainID type) {
        switch (type) {
            case FlatID:
                return std::make_shared<FlatGround>();
                break;
            case BlockID:
                return std::make_shared<Block>();
                break;
            case StairsID:
                return std::make_shared<Stairs>();
                break;
            case GapID:
                return std::make_shared<Gap>();
                break;
            case SlopeID:
                return std::make_shared<Slope>();
                break;
            case ChimneyID:
                return std::make_shared<Chimney>();
                break;
            case ChimneyLRID:
                return std::make_shared<ChimneyLR>();
                break;
            case HillID:
                return std::make_shared<Hill>();
                break;
            case SlopeConstantID:
                return std::make_shared<SlopeConstant>();
                break;
            default:
                assert(false && "Terrain not defined");
                break;
        }

        return nullptr;
    }

    double HeightMap::GetHeight(const Vector3d& pos) const {
        return GetHeight(pos[X], pos[Y]);
    }

    double HeightMap::GetDerivativeOfHeightWrt(Dim2D dim, double x, double y) const {
        switch (dim) {
            case X:
                return GetHeightDerivWrtX(x, y);
            case Y:
                return GetHeightDerivWrtY(x, y);
            default:
                assert(false && "Incorrect dimension");
                return 0.0;
        }
    }

    double HeightMap::GetDerivativeOfHeightWrt(Dim2D dim, const Vector3d& pos) const {
        return GetDerivativeOfHeightWrt(dim, pos[X], pos[Y]);
    }

    double HeightMap::GetFrictionCoeff() const {
        return friction_coeff_;
    }

    HeightMap::Vector3d HeightMap::GetNormalizedBasis(Direction basis, double x,
                                                      double y) const {
        return GetBasis(basis, x, y).normalized();
    }

    HeightMap::Vector3d HeightMap::GetNormalizedBasis(Direction direction,
                                                      const Vector3d& pos) const {
        return GetNormalizedBasis(direction, pos[X], pos[Y]);
    }

    HeightMap::Vector3d HeightMap::GetBasis(Direction basis, double x, double y,
                                            const DimDerivs& deriv) const {
        switch (basis) {
            case Normal:
                return GetNormal(x, y, deriv);
            case Tangent1:
                return GetTangent1(x, y, deriv);
            case Tangent2:
                return GetTangent2(x, y, deriv);
            default:
                assert(false); // basis does not exist
                return Vector3d(0.0, 0.0, 1.0);
        }
    }

    HeightMap::Vector3d HeightMap::GetDerivativeOfNormalizedBasisWrt(
            Direction basis, Dim2D dim, double x, double y) const {
        // inner derivative
        Vector3d dv_wrt_dim = GetBasis(basis, x, y, {dim});

        // outer derivative
        Vector3d v = GetBasis(basis, x, y, {});
        Vector3d dn_norm_wrt_n =
                GetDerivativeOfNormalizedVectorWrtNonNormalizedIndex(v, dim);
        return dn_norm_wrt_n.cwiseProduct(dv_wrt_dim);
    }

    HeightMap::Vector3d HeightMap::GetDerivativeOfNormalizedBasisWrt(
            Direction direction, Dim2D dim, const Vector3d& pos) const {
        return GetDerivativeOfNormalizedBasisWrt(direction, dim, pos[X], pos[Y]);
    }

    HeightMap::Jacobian HeightMap::GetDerivativeOfBasis(Direction direction,
                                                        const Vector3d& pos) const {
        Jacobian dv_dx(3, 3);

        // Derivative w.r.t. z is zero
        for (auto dim : {X_, Y_}) {
            Vector3d dvec = GetBasis(direction, pos[X], pos[Y], {dim});

            for (auto row : {X, Y, Z}) {
                dv_dx.insert(row, dim) = dvec[row];
            }
        }

        return dv_dx;
    }

    HeightMap::Jacobian HeightMap::GetDerivativeOfNormalizedBasis(
            Direction direction, const Vector3d& pos) const {
        Vector3d v = GetBasis(direction, pos[X], pos[Y]);

        Jacobian dvnorm_dv = GetDerivativeOfNormalizedVectorWrtNonNormalized(v);
        Jacobian dv_dx = GetDerivativeOfBasis(direction, pos);

        Jacobian dvnorm_dx = dvnorm_dv * dv_dx;

        return dvnorm_dx;
    }

    HeightMap::Vector3d HeightMap::GetNormal(double x, double y,
                                             const DimDerivs& deriv) const {
        Vector3d n;

        bool basis_requested = deriv.empty();

        for (auto dim : {X_, Y_}) {
            if (basis_requested)
                n(dim) = -GetDerivativeOfHeightWrt(dim, x, y);
            else
                n(dim) = -GetSecondDerivativeOfHeightWrt(dim, deriv.front(), x, y);
        }

        n(Z) = basis_requested ? 1.0 : 0.0;

        return n;
    }

    HeightMap::Vector3d HeightMap::GetTangent1(double x, double y,
                                               const DimDerivs& deriv) const {
        Vector3d tx;

        bool basis_requested = deriv.empty();

        tx(X) = basis_requested ? 1.0 : 0.0;
        tx(Y) = 0.0;
        tx(Z) = basis_requested ?
                GetDerivativeOfHeightWrt(X_, x, y) :
                GetSecondDerivativeOfHeightWrt(X_, deriv.front(), x, y);

        return tx;
    }

    HeightMap::Vector3d HeightMap::GetTangent2(double x, double y,
                                               const DimDerivs& deriv) const {
        Vector3d ty;

        bool basis_requested = deriv.empty();

        ty(X) = 0.0;
        ty(Y) = basis_requested ? 1.0 : 0.0;
        ty(Z) = basis_requested ?
                GetDerivativeOfHeightWrt(Y_, x, y) :
                GetSecondDerivativeOfHeightWrt(Y_, deriv.front(), x, y);
        return ty;
    }

    HeightMap::Vector3d HeightMap::GetDerivativeOfNormalizedVectorWrtNonNormalizedIndex(
            const Vector3d& v, int idx) const {
        // see notebook or http://blog.mmacklin.com/2012/05/04/implicitsprings/
        return 1 / v.squaredNorm()
               * (v.norm() * Vector3d::Unit(idx) - v(idx) * v.normalized());
    }

    HeightMap::Jacobian HeightMap::GetDerivativeOfNormalizedVectorWrtNonNormalized(
            const Vector3d& v) const {
        // see notebook or http://blog.mmacklin.com/2012/05/04/implicitsprings/
        //return 1 / v.squaredNorm() * (v.norm() * Vector3d::Unit(idx) - v(idx) * v.normalized());

        Vector3d vnorm = v.normalized();
        MatrixXd jac_dense = (MatrixXd::Identity(3, 3) - vnorm * vnorm.transpose())
                             * 1 / v.norm();

        return RobotModel::DenseToSparse(jac_dense);
    }

    double HeightMap::GetSecondDerivativeOfHeightWrt(Dim2D dim1, Dim2D dim2,
                                                     double x, double y) const {
        if (dim1 == X_) {
            if (dim2 == X_)
                return GetHeightDerivWrtXX(x, y);
            if (dim2 == Y_)
                return GetHeightDerivWrtXY(x, y);
        } else {
            if (dim2 == X_)
                return GetHeightDerivWrtYX(x, y);
            if (dim2 == Y_)
                return GetHeightDerivWrtYY(x, y);
        }

        assert(false); // second derivative not specified.
        return 0.0;
    }

    // first derivatives that must be implemented by the user
    double HeightMap::GetHeightDerivWrtX(double, double) const {
        return 0.0;
    }

    double HeightMap::GetHeightDerivWrtY(double, double) const {
        return 0.0;
    }

    // second derivatives with respect to first letter, then second
    double HeightMap::GetHeightDerivWrtXX(double, double) const {
        return 0.0;
    }

    double HeightMap::GetHeightDerivWrtXY(double, double) const {
        return 0.0;
    }

    double HeightMap::GetHeightDerivWrtYX(double, double) const {
        return 0.0;
    }

    double HeightMap::GetHeightDerivWrtYY(double, double) const {
        return 0.0;
    }

} /* namespace gambol */
