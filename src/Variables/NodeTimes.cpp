#include <cassert>
#include <gambol/Variables/NodeTimes.h>

namespace gambol {

    // Construct with entire list
    NodeTimes::NodeTimes(const VecTimes& times) {
        for (unsigned int k = 1; k < times.size(); k++) {
            // Make sure times are incremental
            assert(times[k] > times[k - 1]);
        }

        node_times_ = times;
    }

    // Construct with even spacing
    NodeTimes::NodeTimes(double t_total, int n_nodes) {
        double t = 0.0;
        double dt = t_total / static_cast<double>(n_nodes - 1);
        for (int k = 0; k < n_nodes; k++) {
            node_times_.push_back(t);
            t += dt;
        }
    }

    // Get number of nodes
    int NodeTimes::GetNumberOfNodes() const {
        return (int)node_times_.size();
    }

    // Get a single node time
    double NodeTimes::at(int k) const {
        return node_times_.at(k);
    }

    // Get entire list
    const NodeTimes::VecTimes& NodeTimes::GetList() const {
        return node_times_;
    }

    // Get total duration
    double NodeTimes::GetTotalTime() const {
        return node_times_.back();
    }

    // Get last node id based on a time
    int NodeTimes::GetNodeId(double t_des) const {
        if (t_des <= 0.0) {
            return 0;
        }

        int k;
        for (k = 1; k < GetNumberOfNodes(); k++) {
            if (node_times_[k] > t_des) {
                return k - 1; // Get last id _before_ the given time
            }
        }

        return k; // Get last index
    }

    // Get last node and an interpolation factor
    int NodeTimes::GetNodeId(double t_des, double& interpolation) const {
        int k_left = GetNodeId(t_des);

        // Exclude last node
        if (k_left >= GetNumberOfNodes() - 1) {
            k_left = GetNumberOfNodes() - 2;
        }

        int k_right = k_left + 1;

        double factor = (t_des - at(k_left)) / (at(k_right) - at(k_left));

        if (factor > 1.0) {
            factor = 1.0;
        } else if (factor < 0.0) {
            factor = 0.0;
        }
        interpolation = factor;

        return k_left;
    }

    // Get time delta of a node
    double NodeTimes::GetDeltaT(int k) const {
        return this->at(k + 1) - this->at(k);
    }

} /* namespace gambol */
