#include <numeric>
#include <gambol/Variables/PhaseDurations.h>

namespace gambol {

    // Constructor
    PhaseDurations::PhaseDurations(const VecDurations& timings,
                                   bool is_first_phase_in_contact) {
        durations_ = timings;
        t_total_ = std::accumulate(timings.begin(), timings.end(), 0.0);
        initial_contact_state_ = is_first_phase_in_contact;
    }

    // Get durations
    const PhaseDurations::VecDurations& PhaseDurations::GetPhaseDurations() const {
        return durations_;
    }

    // Check state at a given time
    bool PhaseDurations::IsContactPhase(double t_des) const {
        bool contact = initial_contact_state_;

        if (t_des <= 0.0) {
            return contact;
        }

        // Tally over all the states and stop when the given time was reached
        double t = 0.0;
        for (const double& duration : durations_) {
            t += duration; // t is at start of this state

            if (t >= t_des - 1e-5) { // Give a very slight tendency to the left
                break;
            }

            contact = !contact; // Toggle state
        }

        return contact;
    }

} /* namespace gambol */
