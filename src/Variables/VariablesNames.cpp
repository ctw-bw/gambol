#include <gambol/Variables/VariablesNames.h>

namespace gambol {

    namespace id {

        std::string EEForceNodes(uint ee) {
            return forces + std::to_string(ee);
        }

    } // namespace id

} // namespace gambol
