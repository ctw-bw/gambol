#include <gambol/Variables/NodesHolder.h>

namespace gambol {

    // Constructor
    NodesHolder::NodesHolder(const NodesVariables::Ptr& joint_pos,
                             const NodesVariables::Ptr& joint_vel,
                             const NodesVariables::Ptr& torques,
                             const std::vector<NodesVariables::Ptr>& ee_forces,
                             const std::vector<PhaseDurations::Ptr>& phase_durations,
                             const NodeTimes::Ptr& nodes_times) {
        joint_pos_ = joint_pos;
        joint_vel_ = joint_vel;
        torques_ = torques;
        ee_forces_ = ee_forces;
        phase_durations_ = phase_durations;
        node_times_ = nodes_times;
    }

} /* namespace gambol */
