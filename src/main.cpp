#include <gambol/main.h>

#if MODE_GENERATE

#include <memory>
#include <iostream>
#include <thread>
#include <chrono>

#include <Eigen/Dense>

#include <ifopt/problem.h>
#include <ifopt/ipopt_solver.h>
#include <coin/IpReturnCodes.h>

#include <MuJoCoTools/MuJoCoViewer.h>
#include <gambol/RaiSim/RaiSimViewer.h>

#include <gambol/Problem/NlpFormulation.h>
#include <gambol/Variables/NodesHolder.h>
#include <gambol/Robots/Robot.h>
#include <gambol/Robots/MuJoCoRobotModel.h>
#include <gambol/Robots/RaiSimRobotModel.h>
#include <gambol/Terrain/HeightMap.h>
#include <gambol/Generators/GaitGenerator.h>
//#include <gambol/Problem/NLPToFile.h>

/**
 * Run 0 iterations and perform derivative check
 */
#define TEST_NLP false

/**
 * Save NLP result and load it next time as initial guess
 */
#define SAVE_NLP_TO_FILE false

using namespace gambol;

void showMuJoCoSolution(const NodesHolder& solution,
                        const NlpFormulation& formulation);

void showRaiSimSolution(const NodesHolder& solution,
                        const NlpFormulation& formulation);

int main() {
    // Create problem factory
    NlpFormulation formulation;

    auto params = &formulation.params_; // Create pointer for convenience

    // Select robot
    Robot robot(Robot::RaiSimBiped);

    // Choose terrain
    formulation.terrain_ = gambol::HeightMap::MakeTerrain(gambol::HeightMap::FlatID);

    auto solver = std::make_shared<ifopt::IpoptSolver>();
    NlpFormulation::setSolverOptions(solver);
    solver->SetOption("max_cpu_time", 60.0);
    solver->SetOption("max_iter", 300);

    //solver->SetOption("print_timing_statistics", "yes");

    formulation.model_ = robot.robot_model_;
    params->N_ = 11;
    formulation.size_q_ = robot.size_q_;
    formulation.size_dq_ = robot.size_dq_;
    formulation.size_u_ = robot.size_u_;

    // Gait generator

    auto generator = robot.generator_;
    using GG = GaitGenerator;
    //generator->SetCombo(GG::C0);
    std::vector<GG::Gaits> gaits = {GG::Stand, GG::Walk1B, GG::Stand};
    //std::vector<GG::Gaits> gaits = {GG::Stand};
    generator->SetGaits(gaits);

    int steps = generator->GetSteps();
    double step_distance = 0.4;
    double step_time = 1.0;
    double total_duration = step_time * static_cast<double>(steps > 0 ? steps : 1);
    double distance = step_distance * static_cast<double>(steps);

    params->ee_phase_durations_ = generator->GetPhaseDurations(total_duration);
    params->initial_contact_ = generator->IsInContactAtStart();
    params->t_total = params->GetTotalTime();

    // Initial / final config
    formulation.initial_joint_vel_ = formulation.final_joint_vel_ =
            Eigen::VectorXd::Zero(robot.size_dq_);

    formulation.initial_joint_pos_ = robot.basic_joint_pos_;
    formulation.final_joint_pos_ = robot.basic_joint_pos_;
    formulation.final_joint_pos_[0] += distance;
    //formulation.final_joint_pos_[1] += 0.707 * distance; // 3D motion

    formulation.bound_initial_joint_pos_ = robot.bound_initial_joint_pos_;
    formulation.bound_final_joint_pos_ = formulation.bound_initial_joint_pos_;
    formulation.bound_initial_joint_vel_ = robot.bound_initial_joint_vel_;
    formulation.bound_final_joint_vel_ = formulation.bound_initial_joint_vel_;

    formulation.initial_zpos_ = robot.initial_zpos_;

    formulation.bound_symmetry_joint_pos_ = robot.bound_symmetry_joint_pos_;

    // Extra:

    //

    // Joint and torque limits
    formulation.joint_pos_limits_ = robot.joint_pos_limits_;
    formulation.bound_joint_pos_limits_ = robot.bound_joint_pos_limits_;

    formulation.torque_limits_ = robot.torque_limits_;
    formulation.bound_torque_limits_ = robot.bound_torque_limits_;

    // Fire up solver
    NodesHolder solution;
    ifopt::Problem nlp;
    for (const auto& c : formulation.GetVariableSets(solution))
        nlp.AddVariableSet(c);
    for (const auto& c : formulation.GetConstraints(solution))
        nlp.AddConstraintSet(c);
    for (const auto& c : formulation.GetCosts())
        nlp.AddCostSet(c);

    // Print contact phases
    printContactSchedule(solution); // return -1;

#if SAVE_NLP_TO_FILE
    NLPToFile nlp_file(&nlp, "nlp_result.tmp");
    nlp_file.SetGuessFromFile();
#endif

    //auto time_end = std::chrono::high_resolution_clock::now();
    //std::chrono::duration<double> elapsed = time_end - time_start;
    //std::cout << "Elapsed time: " << elapsed.count() * 1000.0 << " ms" << std::endl;
    //return -1;

#if TEST_NLP
    solver->SetOption("derivative_test", "first-order");
    solver->SetOption("point_perturbation_radius", 0.0); // Warning: quaternions will not be unity with random perturbations!
    solver->SetOption("derivative_test_perturbation", 1e-6);
    solver->SetOption("derivative_test_tol", 1e-4);
    solver->SetOption("max_iter", 0);
#endif

    solver->Solve(nlp);

    std::cout.precision(5);
    nlp.PrintCurrent(); // view variable-set, constraint violations, indices,...

#if !TEST_NLP

    printConstraintViolations(nlp);

    printSolution(solution);

    using namespace Ipopt;

    bool solved = false;
    if (solver->GetReturnStatus() == Solve_Succeeded) {
        solved = true;
        std::cout << std::endl << "------ Solution found! ------" << std::endl;
    } else if (solver->GetReturnStatus() == Solved_To_Acceptable_Level) {
        solved = true;
        std::cout << std::endl << "------ Acceptable solution found ------"
                  << std::endl;
    } else {
        std::cout << std::endl << "No proper solution found..." << std::endl;
    }

#if SAVE_NLP_TO_FILE
    if (solved)
    {
        nlp_file.WriteFileFromResult();
    }
#endif
    (void) solved;

    // showMuJoCoSolution(solution, formulation);

    showRaiSimSolution(solution, formulation);

#endif

    return solver->GetReturnStatus();
}

/**
 * Play solution in MuJoCo
 */
void showMuJoCoSolution(const NodesHolder& solution,
                        const NlpFormulation& formulation) {
    auto robot_original = formulation.model_;

    auto robot = std::dynamic_pointer_cast<MuJoCoRobotModel>(robot_original);

    MuJoCoModel* model = &robot->GetMuJoCoModel();
    const mjModel* m = model->get_model();
    mjData* d = model->get_data();

    // Make terrain
    Eigen::MatrixXd map = model->get_heightfield(0, true);
    const double r_x = 5.0, r_y = 5.0;
    for (int r = 0; r < map.rows(); r++) {
        for (int c = 0; c < map.cols(); c++) {
            const double y = ((2.0 * static_cast<double>(r)
                               / static_cast<double>(map.rows() - 1)) - 1.0) * r_x;
            const double x = ((2.0 * static_cast<double>(c)
                               / static_cast<double>(map.cols() - 1)) - 1.0) * r_y;

            map(r, c) = formulation.terrain_->GetHeight(x, y);
        }
    }
    if (!model->set_terrain(0, map, r_x, r_y, 0.5)) {
        std::cout << std::endl << "Failed to create terrain!" << std::endl
                  << std::endl;
    }

    MuJoCoViewer viewer(m);

    // Move camera
    viewer.cam_.distance = 4.0;
    viewer.cam_.lookat[2] += 0.2;
    viewer.cam_.azimuth = 135.0;
    viewer.cam_.elevation = -20.0;

    //viewer.opt_.flags[mjVIS_TRANSPARENT] = 1;

    bool done = false;

    double opt_time = solution.node_times_->GetTotalTime();

    MuJoCoFigure fig_torque("Torques", m->nu);
    MuJoCoFigure fig_forces("Forces", m->nbody);
    MuJoCoFigure fig_joints("Joints", m->nq);
    MuJoCoFigure fig_foot("Foot xz", 3);
    MuJoCoFigure fig_dynamics("Dynamics error", 1);
    int width = 400, height = 200;
    fig_torque.set_size(width, height);
    fig_forces.set_size(width, height);
    fig_joints.set_size(width, height);
    fig_foot.set_size(width, height);
    fig_dynamics.set_size(width, height);

    viewer.add_figure(&fig_torque);
    viewer.add_figure(&fig_forces);
    //viewer.addFigure(&fig_joints);
    viewer.add_figure(&fig_foot);
    viewer.add_figure(&fig_dynamics);

    // As window is still open
    while (!done) {
        double simstart = glfwGetTime();
        double video_time = 0.0;
        mj_resetData(m, d); // Reset simulation

        // Keep looping over solution
        while (video_time <= opt_time + 1.0) // Include a little pause
        {
            double tmstart = glfwGetTime();

            // Progress animation
            video_time = (tmstart - simstart) * 1.0;

            double dynamics_error = 0.0;
            if (video_time < opt_time) {
                const double dt = 0.01;
                double t_2 = video_time + dt;
                robot->SetCurrentTime(solution, t_2);
                Eigen::VectorXd vel_2 = solution.joint_vel_->GetPoint(t_2);
                Eigen::VectorXd acc_2 = robot->GetDynamics();

                robot->SetCurrentTime(solution, video_time);
                Eigen::VectorXd vel_1 = solution.joint_vel_->GetPoint(
                        video_time);
                Eigen::VectorXd acc_1 = robot->GetDynamics();

                Eigen::VectorXd dyn_error = vel_1 - vel_2
                                            + (acc_1 + acc_2) * 0.5 * dt;
                dynamics_error = dyn_error.norm();
            }

            // Quit
            if (viewer.window_should_close()) {
                done = true;
                break;
            }

            Eigen::Vector3d ee_pos = robot->GetEEPos(0);

            double forces[m->nbody];
            for (int i = 0; i < m->nbody; i++) {
                forces[i] = mju_norm3(&d->xfrc_applied[i * 6]);
            }

            fig_torque.add_values(tmstart, d->ctrl);
            fig_forces.add_values(tmstart, forces);
            fig_joints.add_values(tmstart, d->qpos);
            fig_foot.add_values(tmstart, ee_pos.data());
            fig_dynamics.add_value(tmstart, dynamics_error);

            viewer.update_scene(d);

            // Add arrows
            /*for (uint ee = 0; ee < formulation.params_.GetEECount(); ee++)
            {
                auto p_ee = robot->GetEEPos(ee);
                p_ee[1] -= 0.05; // Put outside body
                auto force = solution.ee_forces_[ee]->GetPoint(video_time);

                if (force.norm() > 0.001)
                {
                    force *= 0.005;
                    viewer.draw_arrow(force.data(), p_ee.data());
                }
            }*/

            //viewer.draw_text(std::to_string(video_time));

            viewer.update(); // Render

            // Wait until a total of 1 frame time has passed
            while (glfwGetTime() - tmstart < 1.0 / 60.0) {
                // Sleep for 0.1 millisecond to prevent a busy wait
                MuJoCoViewer::sleep_seconds(0.1 / 1000.0);
            }
        }
    }
}

/**
 * Play solution in RaiSim
 */
void showRaiSimSolution(const NodesHolder& solution,
                        const NlpFormulation& formulation) {

    auto robot_original = formulation.model_;

    auto robot = std::dynamic_pointer_cast<RaiSimRobotModel>(robot_original);

    RaiSimViewer viewer(robot);

    bool done = false;

    double opt_time = solution.node_times_->GetTotalTime();
    double video_time;

    auto next_frame = std::chrono::system_clock::now(); // Use chrono to set frame-rate
    const double dt = 1.0 / 60.0;
    const long dt_micro = (long) (dt * 1.0e6);

    while (!done) { // Keep Repeating as window is still open

        video_time = -0.5; // Reset to start
        // Include a bit of negative time to freeze in the initial frame first

        while (video_time < opt_time + 1.0) { // Play solution (incl. pause at the end)

            //next_frame += std::chrono::microseconds (1000 / 5); // 5Hz
            //next_frame += std::chrono::duration<double>(dt);
            next_frame += std::chrono::microseconds(dt_micro);

            if (video_time < 0.0) {
                robot->SetCurrentTime(solution, 0.0);
            } else if (video_time < opt_time) {
                robot->SetCurrentTime(solution, video_time); // Update scene only during optimization period
            }

            viewer.get_vis()->renderOneFrame();

            if (viewer.get_vis()->getRoot()->endRenderingQueued()) { // If window was closed
                done = true;
                break;
            }

            std::this_thread::sleep_until(next_frame);

            video_time += dt;
        }

    }
}

#endif
