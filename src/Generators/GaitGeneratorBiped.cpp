#include <gambol/Generators/GaitGeneratorBiped.h>

#include <cassert>
#include <iostream>

namespace gambol {

    GaitGeneratorBiped::GaitGeneratorBiped() {
        ContactState init(2, false);
        I_ = b_ = P_ = B_ = init;

        P_.at(L) = true;
        b_.at(R) = true;
        B_ = {true, true};

        SetGaits({Stand});
    }

    void GaitGeneratorBiped::SetCombo(Combos combo) {
        switch (combo) {
            case C0:
                SetGaits({Stand, Walk1, Walk1, Stand});
                break;
            case C1:
                SetGaits({Stand, Run1, Run1, Stand});
                break;
            case C2:
                SetGaits({Stand, Hop1, Hop1, Stand});
                break;
            case C3:
                SetGaits({Stand, Hop1, Hop2, Stand});
                break;
            case C4:
                SetGaits({Stand, Hop5, Hop5, Stand});
                break;
            default:
                assert(false);
                std::cout << "Gait not defined\n";
                break;
        }
    }

    GaitGeneratorBiped::GaitInfo GaitGeneratorBiped::GetGait(Gaits gait) const {
        switch (gait) {
            case Stand:
                return GetStrideStand();
            case Flight:
                return GetStrideFlight();
            case Walk1:
                return GetStrideWalk();
            case Walk1B:
                return GetStrideWalkBegin();
            case Walk1E:
                return GetStrideWalkEnd();
            case Walk2:
                return GetStrideWalk();
            case Run1:
                return GetStrideRun();
            case Run3:
                return GetStrideRun();
            case Hop1:
                return GetStrideHop();
            case Hop2:
                return GetStrideLeftHop();
            case Hop3:
                return GetStrideRightHop();
            case Hop5:
                return GetStrideGallopHop();
            default:
                assert(false); // gait not implemented
        }

        return GaitInfo();
    }

    GaitGeneratorBiped::GaitInfo GaitGeneratorBiped::GetStrideStand() const {
        auto times = {0.2,};
        auto contacts = {B_,};

        return std::make_pair(times, contacts);
    }

    GaitGeneratorBiped::GaitInfo GaitGeneratorBiped::GetStrideFlight() const {
        auto times = {0.5,};
        auto contacts = {I_,};

        return std::make_pair(times, contacts);
    }

    GaitGeneratorBiped::GaitInfo GaitGeneratorBiped::GetStrideWalk() const {
        double step = 0.3;
        double stance = 0.1;
        auto times = {step, stance, step, stance,};
        auto phase_contacts = {b_, B_, // swing left foot
                               P_, B_, // swing right foot
        };

        return std::make_pair(times, phase_contacts);
    }

    GaitGeneratorBiped::GaitInfo GaitGeneratorBiped::GetStrideWalkBegin() const {
        double step = 0.3;
        double stance = 0.1;
        auto times = {0.5 * step, stance, step, stance,};
        auto phase_contacts = {b_, B_, // swing left foot
                               P_, B_, // swing right foot
        };

        return std::make_pair(times, phase_contacts);
    }

    GaitGeneratorBiped::GaitInfo GaitGeneratorBiped::GetStrideWalkEnd() const {
        double step = 0.3;
        double stance = 0.1;
        auto times = {step, stance, 0.5 * step, stance,};
        auto phase_contacts = {b_, B_, // swing left foot
                               P_, B_, // swing right foot
        };

        return std::make_pair(times, phase_contacts);
    }

    GaitGeneratorBiped::GaitInfo GaitGeneratorBiped::GetStrideRun() const {
        double flight = 0.4;
        double pushoff = 0.15;
        double landing = 0.15;
        auto times = {pushoff, flight, landing + pushoff, flight, landing,};
        auto phase_contacts = {b_, I_,     // swing left foot
                               P_, I_, b_, // swing right foot
        };

        return std::make_pair(times, phase_contacts);
    }

    GaitGeneratorBiped::GaitInfo GaitGeneratorBiped::GetStrideHop() const {
        double push = 0.15;
        double flight = 0.5;
        double land = 0.15;
        auto times = {push, flight, land};
        auto phase_contacts = {B_, I_, B_,};

        return std::make_pair(times, phase_contacts);
    }

    GaitGeneratorBiped::GaitInfo GaitGeneratorBiped::GetStrideGallopHop() const {
        double push = 0.2;
        double flight = 0.3;
        double land = 0.2;

        auto times = {push, flight, land, land,};
        auto phase_contacts = {P_, I_, b_, B_,};

        return std::make_pair(times, phase_contacts);
    }

    GaitGeneratorBiped::GaitInfo GaitGeneratorBiped::GetStrideLeftHop() const {
        double push = 0.15;
        double flight = 0.4;
        double land = 0.15;

        auto times = {push, flight, land,};
        auto phase_contacts = {b_, I_, b_};

        return std::make_pair(times, phase_contacts);
    }

    GaitGeneratorBiped::GaitInfo GaitGeneratorBiped::GetStrideRightHop() const {
        double push = 0.2;
        double flight = 0.2;
        double land = 0.2;

        auto times = {push, flight, land};
        auto phase_contacts = {P_, I_, P_};

        return std::make_pair(times, phase_contacts);
    }

} /* namespace gambol */
