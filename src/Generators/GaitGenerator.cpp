#include <gambol/Generators/GaitGenerator.h>

#include <cassert>
#include <numeric> // std::accumulate
#include <algorithm> // std::transform

#include <gambol/Generators/GaitGeneratorMonoped.h>
#include <gambol/Generators/GaitGeneratorBiped.h>
#include <gambol/Generators/GaitGeneratorQuadruped.h>

namespace gambol {

    GaitGenerator::Ptr GaitGenerator::MakeGaitGenerator(int leg_count) {
        switch (leg_count) {
            case 1:
                return std::make_shared<GaitGeneratorMonoped>();
                break;
            case 2:
                return std::make_shared<GaitGeneratorBiped>();
                break;
            case 4:
                return std::make_shared<GaitGeneratorQuadruped>();
                break;
            default:
                assert(false);
                break; // Error: Not implemented
        }

        return nullptr;
    }

    GaitGenerator::VecTimes GaitGenerator::GetPhaseDurations(double t_total,
                                                             EE ee) const {
        // scale total time tu t_total
        std::vector<double> durations;
        for (auto d : GetNormalizedPhaseDurations(ee))
            durations.push_back(d * t_total);

        return durations;
    }

    std::vector<GaitGenerator::VecTimes> GaitGenerator::GetPhaseDurations(
            double T) const {
        std::vector<VecTimes> durations;

        for (EE ee = 0; ee < contacts_.front().size(); ee++) {
            auto duration = GetPhaseDurations(T, ee);
            durations.push_back(duration);
        }

        return durations;
    }

    GaitGenerator::VecTimes GaitGenerator::GetNormalizedPhaseDurations(EE ee) const {
        auto v = GetPhaseDurations().at(ee); // shorthand
        double total_time = std::accumulate(v.begin(), v.end(), 0.0);
        std::transform(v.begin(), v.end(), v.begin(), [total_time](double t_phase) { return t_phase / total_time; });

        return v;
    }

    GaitGenerator::FootDurations GaitGenerator::GetPhaseDurations() const {
        int n_ee = contacts_.front().size();
        VecTimes d_accumulated(n_ee, 0.0);

        FootDurations foot_durations(n_ee);
        for (uint phase = 0; phase < contacts_.size() - 1; ++phase) {
            ContactState curr = contacts_.at(phase);
            ContactState next = contacts_.at(phase + 1);

            for (uint ee = 0; ee < curr.size(); ++ee) {
                d_accumulated.at(ee) += times_.at(phase);

                // if contact will change in next phase, so this phase duration complete
                bool contacts_will_change = curr.at(ee) != next.at(ee);
                if (contacts_will_change) {
                    foot_durations.at(ee).push_back(d_accumulated.at(ee));
                    d_accumulated.at(ee) = 0.0;
                }
            }
        }

        // push back last phase
        for (uint ee = 0; ee < contacts_.back().size(); ++ee)
            foot_durations.at(ee).push_back(d_accumulated.at(ee) + times_.back());

        return foot_durations;
    }

    bool GaitGenerator::IsInContactAtStart(EE ee) const {
        return contacts_.front().at(ee);
    }

    int GaitGenerator::GetSteps(EE ee) const {
        return GetNormalizedPhaseDurations(ee).size() / 2;
    }

    std::vector<bool> GaitGenerator::IsInContactAtStart() const {
        return contacts_.front();
    }

    void GaitGenerator::SetGaits(const std::vector<Gaits>& gaits) {
        contacts_.clear();
        times_.clear();

        for (Gaits g : gaits) {
            auto info = GetGait(g);

            std::vector<double> t = info.first;
            std::vector<ContactState> c = info.second;
            assert(t.size() == c.size()); // make sure every phase has a time

            times_.insert(times_.end(), t.begin(), t.end());
            contacts_.insert(contacts_.end(), c.begin(), c.end());
        }
    }

    GaitGenerator::GaitInfo GaitGenerator::RemoveTransition(const GaitInfo& g) const {
        GaitInfo new_gait = g;

        // remove the final transition between strides
        // but ensure that last step duration is not cut off
        new_gait.first.pop_back();
        new_gait.first.back() += g.first.back();

        new_gait.second.pop_back();

        return new_gait;
    }

} /* namespace gambol */

