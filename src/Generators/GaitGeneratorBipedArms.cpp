#include <gambol/Generators/GaitGeneratorBipedArms.h>

#include <cassert>

namespace gambol {

    GaitGeneratorBipedArms::GaitGeneratorBipedArms() {
        ContactState init(4, false);
        I_ = b_ = P_ = B_ = init;
        I_B_ = P_B_ = b_B_ = B_B_ = init;

        P_.at(L) = P_B_.at(L) = true;
        b_.at(R) = b_B_.at(R) = true;
        B_.at(L) = B_B_.at(L) = B_.at(R) = B_B_.at(R) = true;

        I_B_.at(LH) = b_B_.at(LH) = P_B_.at(LH) = B_B_.at(LH) = true;
        I_B_.at(RH) = b_B_.at(RH) = P_B_.at(RH) = B_B_.at(RH) = true;

        SetGaits({Stand});
    }

    GaitGeneratorBipedArms::GaitInfo GaitGeneratorBipedArms::GetGait(Gaits gait) const {
        switch (gait) {
            case Walk3:
                return GetStrideHandstand();
            default:
                return GaitGeneratorBiped::GetGait(gait);
        }

        return GaitInfo();
    }

    GaitGeneratorBipedArms::GaitInfo GaitGeneratorBipedArms::GetStrideHandstand() const {
        const double stance = 0.15;
        const double bend = 0.05;
        const double lift = 0.15;
        const double balance = 0.3;

        auto times = {stance, bend, lift, balance};
        auto phase_contacts = {B_, b_, b_B_, I_B_};

        return std::make_pair(times, phase_contacts);
    }

} /* namespace gambol */
